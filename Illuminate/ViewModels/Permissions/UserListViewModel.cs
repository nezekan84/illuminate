﻿using Illuminate.ViewModels.User;
using System.Collections.Generic;

namespace Illuminate.ViewModels.Permissions
{
    public class UserListViewModel : BaseViewModel
    {
        public UserListViewModel() : base("Manage Users", "User Permissions")
        {

        }

        public List<UserViewModel> Users { get; set; }
    }
}
