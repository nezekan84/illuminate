﻿namespace Illuminate.ViewModels.Permissions
{
    public class ManagePermissionsViewModel : BaseViewModel
    {
        public ManagePermissionsViewModel() : base("Permission Management", "User Permissions")
        {
        }

        public DataAccess.Models.User User { get; set; }
    }
}
