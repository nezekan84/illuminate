﻿namespace Illuminate.ViewModels.Settings
{
    public class AppSettingsViewModel : BaseViewModel
    {
        public AppSettingsViewModel() : base("Application Settings", "Application Settings")
        {
        }
    }
}
