﻿using Illuminate.DataAccess.Models;
using System.Collections.Generic;

namespace Illuminate.ViewModels.Branch
{
    public class ListingViewModel : BaseViewModel
    {
        public ListingViewModel() : base("Branch Management", "Branch Management")
        {
        }

        public List<Branches> Branches { get; set; }
    }
}
