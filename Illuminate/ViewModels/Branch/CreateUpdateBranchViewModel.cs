﻿using System.ComponentModel.DataAnnotations;

namespace Illuminate.ViewModels.Branch
{
    public class CreateUpdateBranchViewModel : BaseViewModel
    {
        public CreateUpdateBranchViewModel() : base("Branch Management", "Branch Management")
        {
        }

        public int Id { get; set; }

        [Display(Name = "Active")]
        public bool IsActive { get; set; }

        [Display(Name = "Branch")]
        [Required(ErrorMessage = "Name is required.")]
        [StringLength(50)]
        public string Name { get; set; }
    }
}
