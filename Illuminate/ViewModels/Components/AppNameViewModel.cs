﻿namespace Illuminate.ViewModels.Components
{
    public class AppNameViewModel
    {
        public string NameLarge { get; set; }

        public char NameMini { get; set; }
    }
}
