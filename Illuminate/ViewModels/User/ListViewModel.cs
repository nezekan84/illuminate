﻿using System.Collections.Generic;

namespace Illuminate.ViewModels.User
{
    public class ListViewModel : BaseViewModel
    {
        public ListViewModel() : base("Manage Users", "Manage Users")
        {
        }

        public List<UserViewModel> Users { get; set; }
    }
}
