﻿namespace Illuminate.ViewModels.User
{
    public class UserProfileViewModel : BaseViewModel
    {
        public UserProfileViewModel() : base("Profile", "Profile")
        { 
        }

        public UpdateUserViewModel UpdateViewModel { get; set; }
    }
}
