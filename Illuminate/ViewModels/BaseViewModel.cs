﻿namespace Illuminate.ViewModels
{
    public class BaseViewModel
    {
        public string ContentHeader { get; private set; }

        public string PageTitle { get; private set; }

        public BaseViewModel(string pageTitle, string contentHeader)
        {
            ContentHeader = contentHeader;
            PageTitle = pageTitle;
        }
    }
}
