﻿namespace Illuminate.ViewModels.Stock
{
    public class StockViewModel : BaseViewModel
    {
        public StockViewModel() : base("Stock Management", "Stock Management")
        { 
        }

        public int Id { get; set; }

        public string Name { get; set; }
    }
}
