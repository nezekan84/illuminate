﻿using System.Collections.Generic;

namespace Illuminate.ViewModels.Stock
{
    public class StocksListingViewModel : BaseViewModel
    {
        public StocksListingViewModel() : base("Branch Management", "Branch Management")
        {
        }

        public List<StockViewModel> Stocks { get; set; }
    }
}
