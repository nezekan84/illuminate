﻿namespace Illuminate.ViewModels.Stock
{
    public class CreateUpdateStockViewModel : BaseViewModel
    {
        public CreateUpdateStockViewModel() : base("Stock Management", "Stock Management")
        {
        }

        public int Id { get; set; }

        public string Name { get; set; }
    }
}
