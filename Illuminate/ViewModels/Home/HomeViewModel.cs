﻿namespace Illuminate.ViewModels.Home
{
    public class HomeViewModel : BaseViewModel
    {
        public HomeViewModel() : base("Home", "Home")
        {
        }
    }
}
