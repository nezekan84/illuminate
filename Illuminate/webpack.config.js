﻿const path = require('path');
const webpack = require('webpack');

module.exports = {
    mode: 'development',
    watch: true,
    entry: {
        branchManagement: './Async/Containers/BranchManagement.js',
        stockManageView: './Async/Containers/StockManageView.js',
        topnav: './Async/Containers/TopNav.js',
        permissionsManagement: './Async/Containers/PermissionsManagement.js'
    },
    output: {
        path: path.resolve(__dirname, 'wwwroot/app'),
        filename: '[name].js'
    },
    plugins: [
        new webpack.ProvidePlugin({
            $: 'jquery',
            jQuery: 'jquery',
            'window.jquery': 'jquery'
        })
    ],
    module: {
        rules: [{
                test: /\.js?$/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['babel-preset-env', 'babel-preset-es2016', 'babel-preset-react']
                    }
                }
            },
            {
                test: /\.css$/,
                use: ['style-loader', 'css-loader']
            },
            {
                test: /\.less$/,
                loader: 'less-loader'
            },
            {
                test: /\.(png|jpg)$/,
                loader: 'url-loader'
            }
        ]
    }
};