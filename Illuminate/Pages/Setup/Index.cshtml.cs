﻿using Illuminate.Components.Authorization;
using Illuminate.DataAccess.Interfaces;
using Illuminate.DataAccess.Models;
using Illuminate.ViewModels.User;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System;
using System.Reflection;
using System.Threading.Tasks;

namespace Illuminate.Pages.Setup
{
    [AllowAnonymous]
    public class IndexModel : PageModel
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly SignInManager<User> _signInManager;
        private readonly UserManager<User> _userManager;

        [BindProperty]
        public CreateUserViewModel Input { get; set; } = new CreateUserViewModel();

        public IndexModel(IUnitOfWork unitOfWork,
            UserManager<User> userManager,
            SignInManager<User> signInManager,
            RoleManager<IdentityRole> roleManager)
        {
            _unitOfWork = unitOfWork;
            _roleManager = roleManager;
            _userManager = userManager;
            _signInManager = signInManager;
        }

        public async Task<IActionResult> OnGetAsync()
        {
            // prevent others from creating another admin account.
            if (await _unitOfWork.Users.UserCreatedAsync())
                return RedirectToActionPermanent("Index");

            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            // prevent others from creating another admin account.
            if (await _unitOfWork.Users.UserCreatedAsync())
                return RedirectToActionPermanent("Index");

            if (ModelState.IsValid)
            {
                var user = new User
                {
                    FirstName = Input.FirstName,
                    LastName = Input.LastName,
                    UserName = Input.Email,
                    Email = Input.Email,
                    EmailConfirmed = true,
                    IsActive = true // have to set this to true since this is the admin account.
                };

                await SetupClaims();
                var result = await _userManager.CreateAsync(user, Input.Password);

                // ensure that admin claim has been made
                if (!await _roleManager.RoleExistsAsync(UserPermissions.Admin))
                    await _roleManager.CreateAsync(new IdentityRole(UserPermissions.Admin));

                if (result.Succeeded)
                {
                    await _userManager.AddToRoleAsync(user, UserPermissions.Admin);
                    await _signInManager.SignInAsync(user, isPersistent: true);
                    return LocalRedirect("/");
                }
                
                foreach (var error in result.Errors)
                {
                    ModelState.AddModelError(string.Empty, error.Description);
                }
            }

            // If we got this far, something failed, redisplay form
            return Page();
        }

        private async Task SetupClaims()
        {
            // use reflection to get claims (properties)
            var x = typeof(UserPermissions);
            FieldInfo[] claims = x.GetFields();

            foreach(FieldInfo claim in claims)
            {
                // avoid duplicate claims
                var currentClaim = claim.GetValue(claim).ToString();
                if (await _roleManager.RoleExistsAsync(currentClaim))
                    continue;

                try
                {
                    var res = await _roleManager.CreateAsync(
                        new IdentityRole(currentClaim)
                    );
                } 
                catch(Exception e)
                {
                    // TODO: add logging here.
                }
            }
        }
    }
}