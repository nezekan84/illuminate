﻿using AutoMapper;
using Illuminate.ViewModels.User;

namespace Illuminate.Maps.User
{
    public class UserToUpdateViewModel : Profile
    {
        public UserToUpdateViewModel()
        {
            CreateMap<DataAccess.Models.User, UpdateUserViewModel>();
        }
    }
}
