﻿using AutoMapper;
using Illuminate.ViewModels.User;

namespace Illuminate.Maps.User
{
    public class UserModelToCreateViewModelProfile : Profile
    {
        public UserModelToCreateViewModelProfile()
        {
            CreateMap<DataAccess.Models.User, UserViewModel>();
        }
    }
}
