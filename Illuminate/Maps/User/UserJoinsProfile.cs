﻿using AutoMapper;
using Illuminate.DataAccess.Models.Joins;

namespace Illuminate.Maps.User
{
    public class UserJoinsProfile : Profile
    {
        public UserJoinsProfile()
        {
            CreateMap<DataAccess.Models.User, UsersToBranchEmployee>();
        }
    }
}
