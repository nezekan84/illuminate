﻿using AutoMapper;
using Illuminate.ViewModels.User;

namespace Illuminate.Maps.User
{
    public class CreateToUserModelProfile : Profile
    {
        public CreateToUserModelProfile()
        {
            CreateMap<CreateUserViewModel, DataAccess.Models.User>()
                .ForMember(user => user.UserName, opt => opt.MapFrom(viewModel => viewModel.Email));
        }
    }
}
