﻿using AutoMapper;
using Illuminate.DataAccess.Models;
using Illuminate.ViewModels.Branch;

namespace Illuminate.Maps.Branch
{
    public class ModelToCreateUpdateProfile : Profile
    {
        public ModelToCreateUpdateProfile()
        {
            CreateMap<Branches, CreateUpdateBranchViewModel>();
        }
    }
}
