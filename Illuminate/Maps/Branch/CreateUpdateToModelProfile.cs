﻿using AutoMapper;
using Illuminate.DataAccess.Models;
using Illuminate.ViewModels.Branch;

namespace Illuminate.Maps.Branch
{
    public class CreateUpdateToModelProfile : Profile
    {
        public CreateUpdateToModelProfile()
        {
            CreateMap<CreateUpdateBranchViewModel, Branches>();
        }
    }
}
