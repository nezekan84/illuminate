﻿using AutoMapper;
using Illuminate.DataAccess.Models;
using Illuminate.ViewModels.Stock;

namespace Illuminate.Maps.Stock
{
    public class CreateUpdateToStockModelProfile : Profile
    {
        public CreateUpdateToStockModelProfile()
        {
            CreateMap<CreateUpdateStockViewModel, Stocks>();
        }
    }
}
