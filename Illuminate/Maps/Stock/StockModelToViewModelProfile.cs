﻿using AutoMapper;
using Illuminate.DataAccess.Models;
using Illuminate.ViewModels.Stock;

namespace Illuminate.Maps.Stock
{
    public class StockModelToViewModelProfile : Profile
    {
        public StockModelToViewModelProfile()
        {
            CreateMap<Stocks, StockViewModel>();
        }
    }
}
