﻿using AutoMapper;
using Illuminate.DataAccess.Models;
using Illuminate.ViewModels.Stock;

namespace Illuminate.Maps.Stock
{
    public class StockModelToCreateUpdateProfile : Profile
    {
        public StockModelToCreateUpdateProfile()
        {
            CreateMap<Stocks, CreateUpdateStockViewModel>();
        }
    }
}
