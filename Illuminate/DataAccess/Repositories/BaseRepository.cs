﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Illuminate.DataAccess.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace Illuminate.DataAccess.Repositories
{
    public class BaseRepository<TEntity> : IBaseRepository<TEntity> where TEntity : class
    {
        protected readonly ApplicationDbContext AppContext;

        protected readonly DbSet<TEntity> Context;

        public BaseRepository(ApplicationDbContext dbContext)
        {
            AppContext = dbContext;
            Context = dbContext.Set<TEntity>();
        }

        public void Add(TEntity entity)
        {
            Context.Add(entity);
        }

        public void AddRange(IEnumerable<TEntity> entities)
        {
            Context.AddRange(entities);
        }

        public IEnumerable<TEntity> All()
        {
            return Context.ToList();
        }

        public async Task<IEnumerable<TEntity>> AllAsync()
        {
            return await Context.ToListAsync();
        }

        public void Delete(TEntity entity)
        {
            Context.Remove(entity);
        }

        public void DeleteRange(IEnumerable<TEntity> entities)
        {
            Context.RemoveRange(entities);
        }
    }
}
