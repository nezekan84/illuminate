﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Illuminate.DataAccess.Interfaces;
using Illuminate.DataAccess.Models;
using Microsoft.EntityFrameworkCore;

namespace Illuminate.DataAccess.Repositories
{
    public class NotificationRepository : BaseRepository<Notifications>, INotificationRepository
    {
        public NotificationRepository(ApplicationDbContext dbContext) : base(dbContext)
        {
        }

        public async Task<int> CountAllAsync(string userId)
        {
            return await Context.CountAsync(x => x.NotifierId == userId);
        }

        public async Task<int> CountUnreadAsync(string userId)
        {
            return await Context.CountAsync(x => x.NotifierId == userId && x.IsRead == false);
        }

        public async Task<Notifications> FindAsync(Expression<Func<Notifications, bool>> expression)
        {
            return await Context.FirstOrDefaultAsync(expression);
        }

        public async Task<IEnumerable<Notifications>> TakeAsync(string userId, int take, int skip)
        {
            return await Context
                .Include(x => x.NotificationObject)
                .Select(x => new Notifications
                {
                    Id = x.Id,
                    IsRead = x.IsRead,
                    NotificationObject = new NotificationObjects
                    {
                        Id = x.NotificationObject.Id,
                        EntityType = x.NotificationObject.EntityType,
                        EntityId = x.NotificationObject.EntityId,
                        CreatedOn = x.NotificationObject.CreatedOn
                    },
                    NotificationObjectId = x.NotificationObjectId,
                    NotifierId = x.NotifierId
                })                
                .OrderByDescending(x => x.Id)
                .Where(x => x.NotifierId == userId)
                .Skip(skip)
                .Take(take)                
                .ToListAsync();
        }
    }
}
