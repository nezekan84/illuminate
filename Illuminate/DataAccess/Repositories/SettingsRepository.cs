﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Illuminate.DataAccess.Interfaces;
using Illuminate.DataAccess.Models;
using Microsoft.EntityFrameworkCore;

namespace Illuminate.DataAccess.Repositories
{
    public class SettingsRepository : BaseRepository<Settings>, ISettingsRepository
    {
        public SettingsRepository(ApplicationDbContext dbContext) : base(dbContext)
        {
        }

        public async Task<Settings> FindAsync(Expression<Func<Settings, bool>> expression)
        {
            return await Context.FirstOrDefaultAsync(expression);
        }

        public async Task<IEnumerable<Settings>> LoadAppAutoSettingsAsync()
        {
            return await Context
                .Where(x => x.Autoload && x.Entity == Models.Enums.SettingsEntities.App)
                .ToListAsync(); ;
        }

        public async Task<IEnumerable<Settings>> LoadUserAutos(string userId)
        {
            return await Context
                .Where(x => x.UserId == userId && x.Autoload && x.Entity == Models.Enums.SettingsEntities.User)
                .ToListAsync();
        }
    }
}
