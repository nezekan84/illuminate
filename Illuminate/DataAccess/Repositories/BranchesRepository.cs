﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Illuminate.DataAccess.Interfaces;
using Illuminate.DataAccess.Models;
using Microsoft.EntityFrameworkCore;

namespace Illuminate.DataAccess.Repositories
{
    public class BranchesRepository : BaseRepository<Branches>, IBranchesRepository
    {
        public BranchesRepository(ApplicationDbContext dbContext) : base(dbContext)
        {
        }

        public async Task<IEnumerable<Branches>> AllExceptAsync(int id)
        {
            return await Context.Where(x => x.Id != id).ToListAsync();
        }

        public async Task<Branches> FindAsync(Expression<Func<Branches, bool>> expression)
        {            
            // @TODO: add select when table structure is final
            return await Context.FirstOrDefaultAsync(expression);
        }

        public async Task<Branches> GetBranchStateAsync(Expression<Func<Branches, bool>> expression)
        {
            var model = await Context.Select(x => new Branches
            {
                Id = x.Id,
                IsActive = x.IsActive
            }).FirstOrDefaultAsync(expression);

            if (model != null)
                Context.Attach(model);

            return model;
        }
    }
}
