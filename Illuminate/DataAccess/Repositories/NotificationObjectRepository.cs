﻿using Illuminate.DataAccess.Interfaces;
using Illuminate.DataAccess.Models;

namespace Illuminate.DataAccess.Repositories
{
    public class NotificationObjectRepository : BaseRepository<NotificationObjects>, INotificationObjectRepository
    {
        public NotificationObjectRepository(ApplicationDbContext dbContext) : base(dbContext)
        {
        }
    }
}
