﻿using Illuminate.DataAccess.Interfaces;
using Illuminate.DataAccess.Models;
using System;

namespace Illuminate.DataAccess.Repositories
{
    public class BranchStockLogsRepository : BaseRepository<BranchStockLogs>, IBranchStockLogsRepository
    {
        public BranchStockLogsRepository(ApplicationDbContext dbContext) : base(dbContext)
        {
        }

        public void AddLog(BranchStocks branchStock, string userId, bool prevIsActive)
        {
            Context.Add(new BranchStockLogs
            {
                BranchStock = branchStock,
                UserId = userId,
                PreviousIsActive = prevIsActive,
                NewIsActive = !prevIsActive,
                Message = "",
                CreatedOn = DateTime.Now
            });
        }

        public void AddLog(BranchStocks branchStock, string userId, int prevCount, int newCount)
        {
            Context.Add(new BranchStockLogs
            {
                BranchStock = branchStock,
                UserId = userId,
                PreviousCount = prevCount,
                NewCount = newCount,
                Message = "", 
                CreatedOn = DateTime.Now
            });
        }
    }
}
