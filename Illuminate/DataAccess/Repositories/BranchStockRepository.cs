﻿using System;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Illuminate.DataAccess.Interfaces;
using Illuminate.DataAccess.Models;
using Microsoft.EntityFrameworkCore;

namespace Illuminate.DataAccess.Repositories
{
    public class BranchStockRepository : BaseRepository<BranchStocks>, IBranchStockRepository
    {
        public BranchStockRepository(ApplicationDbContext dbContext) : base(dbContext)
        {
        }

        public async Task<BranchStocks> GetLogsAndStockAsync(Expression<Func<BranchStocks, bool>> expression)
        {
            return await Context
                .Include(x => x.Logs)
                    .ThenInclude(logs => logs.User)
                .Include(x => x.Stock)
                .FirstOrDefaultAsync(expression);
        }

        public async Task<BranchStocks> GetLogsAndStockAsync(int id)
        {
            return await Context
                .Include(x => x.Logs)
                    .ThenInclude(logs => logs.User)
                .Include(x => x.Stock)
                .FirstOrDefaultAsync(x => x.Id == id);
        }

        public async Task<BranchStocks> GetOneAsync(Expression<Func<BranchStocks, bool>> expression)
        {
            return await Context.FirstOrDefaultAsync(expression);
        }

        public async Task<BranchStocks> GetOneWithBranchAndStockAsync(Expression<Func<BranchStocks, bool>> expression)
        {
            return await Context.Include(x => x.Branch)
                .Include(x => x.Stock)
                .FirstOrDefaultAsync(expression);
        }

        public async Task<BranchStocks> GetWithStockAsync(Expression<Func<BranchStocks, bool>> expression)
        {
            return await Context.Include(x => x.Stock).FirstOrDefaultAsync(expression);
        }
    }
}
