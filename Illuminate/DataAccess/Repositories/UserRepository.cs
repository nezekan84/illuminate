﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Illuminate.DataAccess.Interfaces;
using Illuminate.DataAccess.Models;
using Illuminate.DataAccess.Models.Joins;
using Microsoft.EntityFrameworkCore;

namespace Illuminate.DataAccess.Repositories
{
    public class UserRepository : BaseRepository<User>, IUserRepository
    {
        public UserRepository(ApplicationDbContext dbContext) : base(dbContext)
        {
        }

        public async Task<IEnumerable<UsersToBranchEmployee>> ForAjaxListingAsync()
        {
            return await Context
                .Include(model => model.Branch)
                .Select(x => new UsersToBranchEmployee
                {
                    Id = x.Id,
                    IsActive = x.IsActive,
                    BranchId = (x.BranchId == null) ? 0 : (int)x.BranchId,
                    BranchName = (x.Branch == null) ? null : x.Branch.Name,
                    FirstName = x.FirstName,
                    LastName = x.LastName,
                    Email = x.Email,
                    PhoneNumber = x.PhoneNumber

                })
                .ToListAsync();
        }

        public async Task<IEnumerable<User>> ForListingAsync()
        {
            return await Context.Select(x => new User
            {
                Id = x.Id,
                IsActive = x.IsActive,
                FirstName = x.FirstName,
                LastName = x.LastName,
                Email = x.Email,
                EmailConfirmed = x.EmailConfirmed
            }).ToListAsync();
        }

        public async Task<int> GetAssignedBranchAsync(string userId)
        {
            var user =  await Context.Select(x => new User
            {
                Id = x.Id,
                BranchId = x.BranchId
            }).FirstOrDefaultAsync(x => x.Id == userId);
            return (int) user.BranchId;
        }

        public async Task<User> GetUserByEmailAsync(string email)
        {
            return await Context.FirstOrDefaultAsync(x => x.Email == email);
        }

        public async Task<IEnumerable<string>> GetUsersFromBranchExceptAsync(int branchId, string exceptUserId)
        {
            var users = await Context
            .Where(x => x.BranchId == branchId && x.Id != exceptUserId)
            .ToListAsync();

            return new List<string>(users.Select(x =>  x.Id));
        }

        public async Task<bool> UserCreatedAsync()
        {
            return await Context.FirstOrDefaultAsync(x => x.UserName != null) != null;
        }
    }
}
