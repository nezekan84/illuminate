﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Illuminate.DataAccess.Interfaces;
using Illuminate.DataAccess.Models;
using Illuminate.DataAccess.Models.Joins;
using Microsoft.EntityFrameworkCore;

namespace Illuminate.DataAccess.Repositories
{
    public class StocksRepository : BaseRepository<Stocks>, IStocksRepository
    {
        public StocksRepository(ApplicationDbContext dbContext) : base(dbContext)
        {
        }

        public async Task<Stocks> FindAsync(Expression<Func<Stocks, bool>> expression)
        {
            return await Context.FirstOrDefaultAsync(expression);
        }

        public async Task<IEnumerable<StocksToBranchStocks>> ForAjaxListingAsync(int branchId)
        {
            var collection = await Context.Include(stock => stock.Branches)
                .Select(x => new Stocks
                {
                    Id = x.Id,
                    Name = x.Name,
                    Branches = x.Branches.Where(brStocks => brStocks.BranchId == branchId).ToList()
                })
                .ToListAsync();

            List<StocksToBranchStocks> transformed = new List<StocksToBranchStocks>();
            if (!collection.Any())
                return transformed;

            foreach (var stock in collection)
            {
                transformed.Add(new StocksToBranchStocks
                {
                    BranchStockId = (stock.Branches.Any()) ? stock.Branches.First().Id : 0,
                    StockId = stock.Id,
                    BranchId = (stock.Branches.Any()) ? stock.Branches.First().BranchId : 0,
                    IsActive = (stock.Branches.Any()) ? stock.Branches.First().IsActive : false,
                    Count = (stock.Branches.Any()) ? stock.Branches.First().Count : 0,
                    Name = stock.Name
                });
            }

            return transformed;
        }
    }
}
