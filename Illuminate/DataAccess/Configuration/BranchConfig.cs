﻿using Illuminate.DataAccess.Models;
using Microsoft.EntityFrameworkCore;

namespace Illuminate.DataAccess.Configuration
{
    public class BranchConfig : BaseConfig<Branches>
    {
        public BranchConfig(ModelBuilder builder) : base(builder)
        {
            Entity.HasMany(branch => branch.Employees)
                .WithOne(user => user.Branch)
                .HasForeignKey(user => user.BranchId)
                .HasPrincipalKey(branch => branch.Id)
                .OnDelete(DeleteBehavior.Restrict);
        }
    }
}
