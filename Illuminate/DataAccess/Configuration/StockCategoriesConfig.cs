﻿using Illuminate.DataAccess.Models;
using Microsoft.EntityFrameworkCore;

namespace Illuminate.DataAccess.Configuration
{
    public class StockCategoriesConfig : BaseConfig<StockCategories>
    {
        public StockCategoriesConfig(ModelBuilder modelBuilder) : base(modelBuilder)
        {
            Entity.ToTable("StockCategories");

            Entity.HasOne(stockCat => stockCat.Category)
                .WithMany(cat => cat.Stocks)
                .HasForeignKey(stockCat => stockCat.CategoryId)
                .HasPrincipalKey(cat => cat.Id)
                .OnDelete(DeleteBehavior.Cascade);

            Entity.HasOne(stockCat => stockCat.Stock)
                .WithMany(stock => stock.Categories)
                .HasForeignKey(stockCat => stockCat.StockId)
                .HasPrincipalKey(stock => stock.Id)
                .OnDelete(DeleteBehavior.Cascade);
        }
    }
}
