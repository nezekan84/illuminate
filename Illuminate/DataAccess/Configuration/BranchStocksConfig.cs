﻿using Illuminate.DataAccess.Models;
using Microsoft.EntityFrameworkCore;

namespace Illuminate.DataAccess.Configuration
{
    public class BranchStocksConfig : BaseConfig<BranchStocks>
    {
        public BranchStocksConfig(ModelBuilder modelBuilder) : base(modelBuilder)
        {
            Entity.ToTable("BranchStocks");

            Entity.HasOne(branchStock => branchStock.Branch)
                .WithMany(branch => branch.Stocks)
                .HasForeignKey(branchStock => branchStock.BranchId)
                .HasPrincipalKey(branch => branch.Id)
                .OnDelete(DeleteBehavior.Cascade);

            Entity.HasOne(branchStock => branchStock.Stock)
                .WithMany(stock => stock.Branches)
                .HasForeignKey(branchStock => branchStock.StockId)
                .HasPrincipalKey(stock => stock.Id)
                .OnDelete(DeleteBehavior.Cascade);

            //Entity.HasMany(branchStock => branchStock.Logs)
            //    .WithOne(logs => logs.BranchStock)
            //    .HasForeignKey(logs => logs.BranchStockId);
        }
    }
}
