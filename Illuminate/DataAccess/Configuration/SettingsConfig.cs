﻿using Illuminate.DataAccess.Models;
using Microsoft.EntityFrameworkCore;

namespace Illuminate.DataAccess.Configuration
{
    public class SettingsConfig : BaseConfig<Settings>
    {
        public SettingsConfig(ModelBuilder builder) : base(builder)
        {
            Entity.HasOne(setting => setting.User)
                .WithMany(user => user.Settings)
                .HasForeignKey(settings => settings.UserId)
                .OnDelete(DeleteBehavior.Cascade);
        }
    }
}
