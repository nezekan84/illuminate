﻿using Illuminate.DataAccess.Models;
using Microsoft.EntityFrameworkCore;

namespace Illuminate.DataAccess.Configuration
{
    public class UsersConfig : BaseConfig<User>
    {
        public UsersConfig(ModelBuilder modelBuilder) : base(modelBuilder)
        {
            Entity.HasMany(user => user.NotificationObjects)
                .WithOne(notifObj => notifObj.Actor)
                .HasForeignKey(notifObj => notifObj.ActorId)
                .HasPrincipalKey(user => user.Id)
                .OnDelete(DeleteBehavior.Cascade);
        }
    }
}
