﻿using Illuminate.DataAccess.Models;
using Microsoft.EntityFrameworkCore;

namespace Illuminate.DataAccess.Configuration
{
    public class NotificationObjectsConfig : BaseConfig<NotificationObjects>
    {
        public NotificationObjectsConfig(ModelBuilder modelBuilder) : base(modelBuilder)
        {
            Entity.HasMany(notifObj => notifObj.Notifications)
                .WithOne(notif => notif.NotificationObject)
                .HasForeignKey(notif => notif.NotificationObjectId)
                .HasPrincipalKey(notifObj => notifObj.Id)
                .OnDelete(DeleteBehavior.Cascade);
        }
    }
}
