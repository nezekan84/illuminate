﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Illuminate.DataAccess.Configuration
{
    public class BaseConfig<T> where T : class
    {
        /// <summary>
        /// Use this to configure table via Fluent API
        /// </summary>
        protected EntityTypeBuilder<T> Entity { get; private set; }

        public BaseConfig(ModelBuilder builder)
        {
            Entity = builder.Entity<T>();
        }
    }
}
