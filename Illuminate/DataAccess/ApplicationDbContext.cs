﻿using Illuminate.DataAccess.Configuration;
using Illuminate.DataAccess.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace Illuminate.DataAccess
{
    public class ApplicationDbContext : IdentityDbContext<User>
    {
        public DbSet<Branches> Branches { get; set; }        

        public DbSet<BranchStocks> BranchStocks { get; set; }

        public DbSet<BranchStockLogs> BranchStockLogs { get; set; }

        public DbSet<Categories> Categories { get; set; }

        public DbSet<NotificationObjects> NotificationObjects { get; set; }

        public DbSet<Notifications> Notifications { get; set; }

        public DbSet<Settings> Settings { get; set; }

        public DbSet<StockCategories> StockCategories { get; set; }        

        public DbSet<Stocks> Stocks { get; set; }

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        { }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            new BranchStocksConfig(builder);
            new NotificationObjectsConfig(builder);
            new SettingsConfig(builder);
            new StockCategoriesConfig(builder);
            new UsersConfig(builder);

            base.OnModelCreating(builder);
        }
    }
}
