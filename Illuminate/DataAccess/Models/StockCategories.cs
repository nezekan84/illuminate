﻿namespace Illuminate.DataAccess.Models
{
    public class StockCategories
    {
        public int Id { get; set; }

        public int CategoryId { get; set; }

        public Categories Category { get; set; }

        public int StockId { get; set; }

        public Stocks Stock { get; set; }
    }
}
