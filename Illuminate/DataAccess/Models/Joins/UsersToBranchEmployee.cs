﻿namespace Illuminate.DataAccess.Models.Joins
{
    public class UsersToBranchEmployee
    {
        public string Id { get; set; }

        public bool IsActive { get; set; }

        public int BranchId { get; set; }

        public string BranchName { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Email { get; set; }

        public string PhoneNumber { get; set; }
    }
}
