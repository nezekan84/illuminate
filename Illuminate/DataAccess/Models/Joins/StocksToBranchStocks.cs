﻿namespace Illuminate.DataAccess.Models.Joins
{
    public class StocksToBranchStocks
    {
        public int BranchStockId { get; set; }

        public int StockId { get; set; }

        public int BranchId { get; set; }

        public bool IsActive { get; set; }

        public int Count { get; set; }

        public string Name { get; set; }
    }
}
