﻿using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Illuminate.DataAccess.Models
{
    public class User : IdentityUser
    {
        [PersonalData]
        [Required]
        [StringLength(50)]
        public string FirstName { get; set; }

        [PersonalData]
        [Required]
        [StringLength(50)]
        public string LastName { get; set; }

        public bool IsActive { get; set; }

        public int? BranchId { get; set; }

        public virtual Branches Branch { get; set; }

        public List<Notifications> Notifications { get; set; }

        public List<NotificationObjects> NotificationObjects { get; set; }

        public List<Settings> Settings { get; set; }
    }
}
