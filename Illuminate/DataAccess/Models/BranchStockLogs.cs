﻿using System;

namespace Illuminate.DataAccess.Models
{
    public class BranchStockLogs
    {
        public int Id { get; set; }

        public int BranchStockId { get; set; }

        public BranchStocks BranchStock { get; set; }

        public string UserId { get; set; }

        public User User { get; set; }

        public bool PreviousIsActive { get; set; }

        public bool NewIsActive { get; set; }

        public int PreviousCount { get; set; }

        public int NewCount { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Message { get; set; }

        public DateTime CreatedOn { get; set; }
    }
}
