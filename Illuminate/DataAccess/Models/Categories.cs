﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Illuminate.DataAccess.Models
{
    public class Categories
    {
        public int Id { get; set; }

        [Required]
        [StringLength(50)]
        public string Name { get; set; }

        public List<StockCategories> Stocks { get; set; }
    }
}
