﻿using System.Collections.Generic;

namespace Illuminate.DataAccess.Models
{
    public class Branches
    {
        public int Id { get; set; }

        public bool IsActive { get; set; }

        public string Name { get; set; }

        public List<User> Employees { get; set; }

        public List<BranchStocks> Stocks { get; set; }
    }
}
