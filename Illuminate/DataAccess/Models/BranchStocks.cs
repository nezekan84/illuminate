﻿using System.Collections.Generic;

namespace Illuminate.DataAccess.Models
{
    public class BranchStocks
    {
        public int Id { get; set; }

        public int BranchId { get; set; }

        public bool IsActive { get; set; }

        public Branches Branch { get; set; }

        public int StockId { get; set; }

        public Stocks Stock { get; set; }

        public int Count { get; set; }

        public List<BranchStockLogs> Logs { get; set; }
    }
}
