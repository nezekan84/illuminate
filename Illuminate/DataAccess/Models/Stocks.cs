﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Illuminate.DataAccess.Models
{
    public class Stocks
    {
        public int Id { get; set; }

        [Required]
        [StringLength(128)]
        public string Name { get; set; }

        public List<BranchStocks> Branches { get; set; }

        public List<StockCategories> Categories { get; set; }
    }
}
