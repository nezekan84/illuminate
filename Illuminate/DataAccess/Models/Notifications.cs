﻿namespace Illuminate.DataAccess.Models
{
    public class Notifications
    {
        public int Id { get; set; }

        public int NotificationObjectId { get; set; }

        public NotificationObjects NotificationObject { get; set; }

        /// <summary>
        /// Destination user/user that gets notified
        /// </summary>
        public string NotifierId { get; set; }

        public User Notifier { get; set; }

        public bool IsRead { get; set; }
    }
}
