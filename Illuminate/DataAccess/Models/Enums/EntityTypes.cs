﻿using System.ComponentModel.DataAnnotations;

namespace Illuminate.DataAccess.Models.Enums
{
    public enum EntityTypes
    {
        /// <summary>
        /// Activate stock
        /// </summary>
        [Display(Name = "Activate stock")]
        StockActivate = 0,

        /// <summary>
        /// Deactivate stock
        /// </summary>
        [Display(Name = "Deactivate stock")]
        StockRemove = 1,

        /// <summary>
        /// Add to stocks
        /// </summary>
        [Display(Name = "Increase stock")]
        StockCountUp = 2,

        /// <summary>
        /// Remove from stocks
        /// </summary>
        [Display(Name = "Decrease stock")]
        StockCountDown = 3,

        /// <summary>
        /// Add user to branch
        /// </summary>
        [Display(Name = "Add user to branch")]
        BranchUserAdd = 4,

        /// <summary>
        /// Remove user from branch
        /// </summary>
        [Display(Name = "Remove user from branch")]
        BranchUserRemove = 5
    }
}
