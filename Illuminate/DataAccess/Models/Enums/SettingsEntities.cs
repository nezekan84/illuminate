﻿namespace Illuminate.DataAccess.Models.Enums
{
    /// <summary>
    /// 
    /// </summary>
    public enum SettingsEntities
    {
        /// <summary>
        /// Inidicates that the record will be used for application wide setting
        /// </summary>
        App = 0,

        /// <summary>
        /// Indicates that the record will be used only for a specific user
        /// </summary>
        User = 1
    }
}
