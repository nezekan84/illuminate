﻿using System.ComponentModel.DataAnnotations;

namespace Illuminate.DataAccess.Models.Enums
{
    /// <summary>
    /// 
    /// </summary>
    public enum SettingsAttributes
    {
        [Display(Name = "Application Name", GroupName = "Application")]
        AppName = 0,

        [Display(Name = "Low stock count", GroupName = "Stock - Notifications")]
        StockLow = 100,

        [Display(Name = "Empty stock", GroupName = "Stock - Notifications")]
        StockEmpty = 101
    }
}
