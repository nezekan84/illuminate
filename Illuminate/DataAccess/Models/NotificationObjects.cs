﻿using Illuminate.DataAccess.Models.Enums;
using System;
using System.Collections.Generic;

namespace Illuminate.DataAccess.Models
{
    public class NotificationObjects
    {
        public int Id { get; set; }

        public string ActorId { get; set; }

        /// <summary>
        /// Creator of notification
        /// </summary>
        public User Actor { get; set; }

        public EntityTypes EntityType { get; set; }

        public int EntityId { get; set; }

        public DateTime CreatedOn { get; set; }

        public List<Notifications> Notifications { get; set; }
    }
}
