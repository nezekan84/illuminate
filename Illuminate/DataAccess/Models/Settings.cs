﻿using Illuminate.DataAccess.Models.Enums;

namespace Illuminate.DataAccess.Models
{
    public class Settings
    {
        public int Id { get; set; }

        public string UserId { get; set; }

        public User User { get; set; }

        public bool Autoload { get; set; } = false;

        public SettingsEntities Entity { get; set; }

        public SettingsAttributes Attribute { get; set; }

        public string Value { get; set; }
    }
}
