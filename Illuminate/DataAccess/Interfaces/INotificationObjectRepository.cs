﻿using Illuminate.DataAccess.Models;

namespace Illuminate.DataAccess.Interfaces
{
    public interface INotificationObjectRepository : IBaseRepository<NotificationObjects>
    {
    }
}
