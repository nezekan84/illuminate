﻿using Illuminate.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Illuminate.DataAccess.Interfaces
{
    public interface ISettingsRepository : IBaseRepository<Settings>
    {
        /// <summary>
        /// Search for one setting
        /// </summary>
        /// <param name="expression"></param>
        /// <returns></returns>
        Task<Settings> FindAsync(Expression<Func<Settings, bool>> expression);

        /// <summary>
        /// Get all app auto load settings
        /// </summary>
        /// <returns></returns>
        Task<IEnumerable<Settings>> LoadAppAutoSettingsAsync();

        /// <summary>
        /// Get all user auto load settings
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        Task<IEnumerable<Settings>> LoadUserAutos(string userId);
    }
}
