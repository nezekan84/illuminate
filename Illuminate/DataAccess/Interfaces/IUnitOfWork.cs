﻿using System;
using System.Threading.Tasks;

namespace Illuminate.DataAccess.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        /// <summary>
        /// Branches repo
        /// </summary>
        IBranchesRepository Branches { get; }

        /// <summary>
        /// Branch stock logs repo
        /// </summary>
        IBranchStockLogsRepository BranchStockLogs { get; }

        /// <summary>
        /// Branch stocks repo
        /// </summary>
        IBranchStockRepository BranchStocks { get; }

        /// <summary>
        /// Notification objects repo
        /// </summary>
        INotificationObjectRepository NotificationObjects { get; }

        /// <summary>
        /// Notifications repo
        /// </summary>
        INotificationRepository Notifications { get; }

        /// <summary>
        /// Settings repo
        /// </summary>
        ISettingsRepository Settings { get; }

        /// <summary>
        /// Stocks repo
        /// </summary>
        IStocksRepository Stocks { get; }

        /// <summary>
        /// Users repo
        /// </summary>
        IUserRepository Users { get; }

        /// <summary>
        /// Save changes
        /// </summary>
        /// <returns></returns>
        int Save();

        /// <summary>
        /// (ASYNC) Save changes
        /// </summary>
        /// <returns></returns>
        Task<int> SaveAsync();
    }
}
