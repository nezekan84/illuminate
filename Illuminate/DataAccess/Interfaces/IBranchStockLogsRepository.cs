﻿using Illuminate.DataAccess.Models;

namespace Illuminate.DataAccess.Interfaces
{
    public interface IBranchStockLogsRepository : IBaseRepository<BranchStockLogs>
    {
        /// <summary>
        /// Add new activation/deactivation log
        /// </summary>
        /// <param name="branchStock"></param>
        /// <param name="userId"></param>
        /// <param name="prevIsActive"></param>
        void AddLog(BranchStocks branchStock, string userId, bool prevIsActive);

        /// <summary>
        /// Add new count movement log
        /// </summary>
        /// <param name="branchStock"></param>
        /// <param name="userId"></param>
        /// <param name="prevCount"></param>
        /// <param name="newCount"></param>
        void AddLog(BranchStocks branchStock, string userId, int prevCount, int newCount);
    }
}
