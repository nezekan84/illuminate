﻿using Illuminate.DataAccess.Models;
using Illuminate.DataAccess.Models.Joins;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Illuminate.DataAccess.Interfaces
{
    public interface IUserRepository : IBaseRepository<User>
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        Task<IEnumerable<UsersToBranchEmployee>> ForAjaxListingAsync();

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        Task<IEnumerable<User>> ForListingAsync();

        /// <summary>
        /// Get branch id of user
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        Task<int> GetAssignedBranchAsync(string userId);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        Task<User> GetUserByEmailAsync(string email);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="branchId"></param>
        /// <param name="exceptUserId"></param>
        /// <returns></returns>
        Task<IEnumerable<string>> GetUsersFromBranchExceptAsync(int branchId, string exceptUserId);

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        Task<bool> UserCreatedAsync();
    }
}
