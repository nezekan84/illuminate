﻿using Illuminate.DataAccess.Models;
using Illuminate.DataAccess.Models.Joins;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Illuminate.DataAccess.Interfaces
{
    public interface IStocksRepository : IBaseRepository<Stocks>
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="expression"></param>
        /// <returns></returns>
        Task<Stocks> FindAsync(Expression<Func<Stocks, bool>> expression);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="branchId"></param>
        /// <returns></returns>
        Task<IEnumerable<StocksToBranchStocks>> ForAjaxListingAsync(int branchId);
    }
}
