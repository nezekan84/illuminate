﻿using Illuminate.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Illuminate.DataAccess.Interfaces
{
    public interface IBranchesRepository : IBaseRepository<Branches>
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<IEnumerable<Branches>> AllExceptAsync(int id);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="expression"></param>
        /// <returns></returns>
        Task<Branches> FindAsync(Expression<Func<Branches, bool>> expression);

        /// <summary>
        /// Get branch Id and IsActive
        /// </summary>
        /// <param name="expression"></param>
        /// <returns></returns>
        Task<Branches> GetBranchStateAsync(Expression<Func<Branches, bool>> expression);
    }
}
