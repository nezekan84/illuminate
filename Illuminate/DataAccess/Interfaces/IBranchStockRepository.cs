﻿using Illuminate.DataAccess.Models;
using System;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Illuminate.DataAccess.Interfaces
{
    public interface IBranchStockRepository : IBaseRepository<BranchStocks>
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="expression"></param>
        /// <returns></returns>
        Task<BranchStocks> GetLogsAndStockAsync(Expression<Func<BranchStocks, bool>> expression);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<BranchStocks> GetLogsAndStockAsync(int id);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="expression"></param>
        /// <returns></returns>
        Task<BranchStocks> GetOneAsync(Expression<Func<BranchStocks, bool>> expression);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="expression"></param>
        /// <returns></returns>
        Task<BranchStocks> GetOneWithBranchAndStockAsync(Expression<Func<BranchStocks, bool>> expression);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="expression"></param>
        /// <returns></returns>
        Task<BranchStocks> GetWithStockAsync(Expression<Func<BranchStocks, bool>> expression);
    }
}
