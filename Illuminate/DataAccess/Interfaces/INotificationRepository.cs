﻿using Illuminate.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Illuminate.DataAccess.Interfaces
{
    public interface INotificationRepository : IBaseRepository<Notifications>
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        Task<int> CountAllAsync(string userId);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        Task<int> CountUnreadAsync(string userId);

        /// <summary>
        /// Query for a specific notification
        /// </summary>
        /// <param name="expression"></param>
        /// <returns></returns>
        Task<Notifications> FindAsync(Expression<Func<Notifications, bool>> expression);
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="take"></param>
        /// <param name="skip"></param>
        /// <returns></returns>
        Task<IEnumerable<Notifications>> TakeAsync(string userId, int take, int skip);
    }
}
