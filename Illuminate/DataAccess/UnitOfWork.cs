﻿using System.Threading.Tasks;
using Illuminate.DataAccess.Interfaces;
using Illuminate.DataAccess.Repositories;

namespace Illuminate.DataAccess
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly ApplicationDbContext _context;
        
        public IBranchesRepository Branches { get; }

        public IBranchStockLogsRepository BranchStockLogs { get; }

        public IBranchStockRepository BranchStocks { get; }        

        public INotificationObjectRepository NotificationObjects { get; }

        public INotificationRepository Notifications { get; }

        public ISettingsRepository Settings { get; }

        public IStocksRepository Stocks { get; }

        public IUserRepository Users { get; }

        public UnitOfWork(ApplicationDbContext dbContext)
        {
            _context = dbContext;

            Branches = new BranchesRepository(_context);
            BranchStockLogs = new BranchStockLogsRepository(_context);
            BranchStocks = new BranchStockRepository(_context);            
            NotificationObjects = new NotificationObjectRepository(_context);
            Notifications = new NotificationRepository(_context);
            Settings = new SettingsRepository(_context);
            Stocks = new StocksRepository(_context);
            Users = new UserRepository(_context);
        }

        public int Save()
        {
            return _context.SaveChanges();
        }

        public async Task<int> SaveAsync()
        {
            return await _context.SaveChangesAsync();
        }

        public void Dispose()
        {
            _context.Dispose();
        }
    }
}
