﻿using AutoMapper;
using Illuminate.Async.DTO.User;
using Illuminate.Async.Responses.Branch;
using Illuminate.Async.Responses.User;
using Illuminate.Components.Authorization;
using Illuminate.DataAccess.Interfaces;
using Illuminate.DataAccess.Models;
using Illuminate.DataAccess.Models.Joins;
using Illuminate.ViewModels.User;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Illuminate.Controllers
{
    [AuthorizeRoles(true, UserPermissions.ManageUsers)]
    public class UserController : Controller
    {
        private readonly IMapper _mapper;

        private readonly IUnitOfWork _unitOfWork;

        private readonly UserManager<User> _userManager;

        public UserController(IMapper mapper,
            IUnitOfWork unitOfWork,
            UserManager<User> userManager)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
            _userManager = userManager;
        }

        /// <summary>
        /// list all uers
        /// </summary>
        /// <returns></returns>
        public async Task<ViewResult> Index()
        {
            var collection = await _unitOfWork.Users.ForListingAsync();
            var mappedCollection = _mapper.Map<IEnumerable<User>, List<UserViewModel>>(collection);
            
            return View(new ListViewModel {
                Users = mappedCollection
            });
        }

        /// <summary>
        /// view user profile
        /// Identity/Account/Manage will be used for personal profiles
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<IActionResult> Profile(string id)
        {
            var user = await _userManager.FindByIdAsync(id);
            if (user == null) // redirect if not found
                return RedirectToAction("Index");            

            return View(new UserProfileViewModel
            {
                UpdateViewModel = _mapper.Map<User, UpdateUserViewModel>(user)
            });
        }

        /// <summary>
        /// http post (update) function for Profile()
        /// </summary>
        /// <param name="viewModel"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Update(UserProfileViewModel viewModel)
        {
            if (!ModelState.IsValid)
                return View("Profile", viewModel);
            
            var user = await _userManager.FindByIdAsync(viewModel.UpdateViewModel.Id);
            user.Email = viewModel.UpdateViewModel.Email;
            user.FirstName = viewModel.UpdateViewModel.FirstName;
            user.LastName = viewModel.UpdateViewModel.LastName;
            user.PhoneNumber = viewModel.UpdateViewModel.PhoneNumber;
            await _unitOfWork.SaveAsync();

            return RedirectToAction("Profile", new { id = viewModel.UpdateViewModel.Id });
        }

        /// <summary>
        /// activate/deactive user account
        /// </summary>
        /// <param name="viewModel"></param>
        /// <returns></returns>
        public async Task<IActionResult> SetActive(UserViewModel viewModel)
        {
            var model = await _userManager.FindByIdAsync(viewModel.Id);
            if (model == null)
                return RedirectToAction("Index");

            model.IsActive = !model.IsActive;
            await _userManager.UpdateAsync(model);

            return RedirectToAction("Index");
        }

        /// <summary>
        /// get all users for listing
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<JsonResult> AllUsers()
        {
            var collection = await _unitOfWork.Users.ForAjaxListingAsync();
            return Json(_mapper.Map<List<UsersToBranchEmployee>, List<UserDetailsDto>>((List<UsersToBranchEmployee>) collection));
        }

        /// <summary>
        /// assign user to branch
        /// </summary>
        /// <param name="viewModel"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<JsonResult> AssignToBranch([FromBody] AssignUserDto viewModel)
        {
            var user = await _userManager.FindByIdAsync(viewModel.UserId);
            if (user == null)
                return Json(new UserNotFoundResponse());

            var branch = await _unitOfWork.Branches.FindAsync(x => x.Id == viewModel.BranchId);
            if (branch == null)
                return Json(new BranchNotFoundResponse());

            if (!viewModel.Remove)
            {
                user.BranchId = viewModel.BranchId;
                await _userManager.UpdateAsync(user);
                return Json(new UserAssignedToBranchResponse(branch.Name, user.FirstName, user.LastName));
            }
            else
            {
                user.BranchId = null;
                await _userManager.UpdateAsync(user);
                return Json(new UserRemovedFromBranchResponse(branch.Name, user.FirstName, user.LastName));
            }
        }
    }
}