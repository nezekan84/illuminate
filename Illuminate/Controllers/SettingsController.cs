﻿using Illuminate.Components.Authorization;
using Illuminate.Components.Settings;
using Illuminate.DataAccess.Models;
using Illuminate.ViewModels.Settings;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Illuminate.Controllers
{
    public class SettingsController : Controller
    {
        private readonly ISettingsProxy<Settings> _settingsProxy;

        public SettingsController(ISettingsProxy<Settings> settingsProxy)
        {
            _settingsProxy = settingsProxy;
        }

        public async Task<IActionResult> Index()
        {
            await _settingsProxy.CreateOrUpdateAppSettingAsync(DataAccess.Models.Enums.SettingsAttributes.AppName, "Clarity", true);
            return View();
        }

        [AuthorizeRoles(true, UserPermissions.EditAppSettings)]
        public IActionResult Admin()
        {
            return View(new AppSettingsViewModel());
        }
    }
}