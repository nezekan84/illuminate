﻿using AutoMapper;
using Illuminate.Async.DTO.Stock;
using Illuminate.Async.Responses.Stock;
using Illuminate.Components;
using Illuminate.Components.Authorization;
using Illuminate.Components.Notification;
using Illuminate.DataAccess.Interfaces;
using Illuminate.DataAccess.Models;
using Illuminate.ViewModels.Stock;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Illuminate.Controllers
{
    public class StockController : Controller
    {
        private readonly IMapper _mapper;

        private readonly INotificationService _notification;

        private readonly IUnitOfWork _unitOfWork;

        public StockController(IMapper mapper,
            INotificationService notification,
            IUnitOfWork unitOfWork)
        {
            _mapper = mapper;
            _notification = notification;
            _unitOfWork = unitOfWork;
        }

        /// <summary>
        /// list all stocks
        /// </summary>
        /// <returns></returns>
        [AuthorizeRoles(true, UserPermissions.ListAndViewStocks)]
        public async Task<IActionResult> Index()
        {
            var collection = await _unitOfWork.Stocks.AllAsync();
            
            return View(new StocksListingViewModel()
            {
                Stocks = (List<StockViewModel>) _mapper.Map<IEnumerable<Stocks>, IEnumerable<StockViewModel>>(collection)
            });
        }

        /// <summary>
        /// create view
        /// </summary>
        /// <returns></returns>
        [AuthorizeRoles(true, UserPermissions.CreateUpdateStocks)]
        public ViewResult Create()
        {
            return View(new CreateUpdateStockViewModel());
        }

        /// <summary>
        /// http post (create) function for Create()
        /// </summary>
        /// <param name="viewModel"></param>
        /// <returns></returns>3
        [AuthorizeRoles(true, UserPermissions.CreateUpdateStocks)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> OnCreate(CreateUpdateStockViewModel viewModel)
        {
            if (!ModelState.IsValid)
                return View("Create", viewModel);

            var model = new Stocks();
            _mapper.Map(viewModel, model);
            _unitOfWork.Stocks.Add(model);
            await _unitOfWork.SaveAsync();

            return RedirectToAction("View", new { id = model.Id });
        }

        /// <summary>
        /// view stock
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [AuthorizeRoles(true, UserPermissions.ListAndViewStocks)]
        public async Task<IActionResult> View(int id)
        {
            var model = await _unitOfWork.Stocks.FindAsync(x => x.Id == id);
            if (model == null)
                return RedirectToAction("Index");

            var viewModel = new StockViewModel();
            _mapper.Map(model, viewModel);
            return View(viewModel);
        }

        /// <summary>
        /// http post (update) function for View()
        /// </summary>
        /// <param name="viewModel"></param>
        /// <returns></returns>
        [AuthorizeRoles(true, UserPermissions.CreateUpdateStocks)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Update(CreateUpdateStockViewModel viewModel)
        {
            if (!ModelState.IsValid)
                return View("View", viewModel);

            var model = await _unitOfWork.Stocks.FindAsync(x => x.Id == viewModel.Id);
            _mapper.Map(viewModel, model);
            await _unitOfWork.SaveAsync();

            return RedirectToAction("View", new { id = viewModel.Id });
        }

        /// <summary>
        /// get all branches for listing
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [AuthorizeRoles(true, UserPermissions.ListAndViewStocks)]
        public async Task<JsonResult> ForBranch(int id)
        {
            return Json(await _unitOfWork.Stocks.ForAjaxListingAsync(id));
        }

        /// <summary>
        /// assign stock to branch
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [AuthorizeRoles(true, UserPermissions.AddStocks)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<JsonResult> AssignStock([FromBody] AssignStockDto dto)
        {            
            // avoid possible duplicate.
            var branchStock = await _unitOfWork.BranchStocks
                .GetWithStockAsync(x => x.StockId == dto.StockId && x.BranchId == dto.BranchId);
                            
            if (dto.Remove)
            {
                _unitOfWork.BranchStocks.Delete(branchStock);
                await _unitOfWork.SaveAsync();

                return Json(new StockRemovedResponse(branchStock.Stock.Name));
            }
            
            // assume someone has assigned stock before you.
            if (branchStock != null)
                return Json(new StockAssignedResponse(branchStock.Id, branchStock.Stock.Name));

            var model = new BranchStocks
            {
                BranchId = dto.BranchId,
                StockId = dto.StockId,
                IsActive = false,
                Count = 0
            };
            _unitOfWork.BranchStocks.Add(model);
            await _unitOfWork.SaveAsync();
            var stock = await _unitOfWork.Stocks.FindAsync(x => x.Id == dto.StockId);

            return Json(new StockAssignedResponse(model.Id, stock.Name));
        }

        /// <summary>
        /// activate/deactivate stock
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [AuthorizeRoles(true, UserPermissions.ActivateStocks)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<JsonResult> ActivateStock([FromBody] ActivateStockDto dto)
        {
            var assignedStock = await _unitOfWork.BranchStocks
                .GetWithStockAsync(x => x.Id == dto.BranchStockId);

            if (assignedStock == null)
                return Json(new StockErrorResponse()); // missing record 

            assignedStock.IsActive = dto.Activate;
            await _unitOfWork.SaveAsync();

            string userId = User.GetUserId();

            _unitOfWork.BranchStockLogs.AddLog(assignedStock, userId, !assignedStock.IsActive);
            await _unitOfWork.SaveAsync();
            await _notification.ActivateStockNotification(userId, assignedStock.Id, assignedStock.BranchId);
             return dto.Activate ? 
                Json(new StockActivateResponse(assignedStock.Stock.Name)) 
                : Json(new StockDeactivateResponse(assignedStock.Stock.Name));
        }
        
        /// <summary>
        /// increase stock count
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [AuthorizeRoles(true, UserPermissions.AddStocks)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<JsonResult> AddStock([FromBody] ChangeStockCountDto dto)
        {
            var assignedStock = await _unitOfWork.BranchStocks
                .GetWithStockAsync(x => x.Id == dto.BranchStockId);

            if (assignedStock == null)
                return Json(new StockErrorResponse());

            var prevCount = assignedStock.Count;
            assignedStock.Count = dto.Count;

            _unitOfWork.BranchStockLogs.AddLog(assignedStock, User.GetUserId(), prevCount, dto.Count);
            await _unitOfWork.SaveAsync();

            return Json(new StockCountUpdateResponse(assignedStock.Stock.Name, dto.Count));
        }

        /// <summary>
        /// pull stocks
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [AuthorizeRoles(true, UserPermissions.PullStocks)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<JsonResult> PullStock([FromBody] ChangeStockCountDto dto)
        {
            var assignedStock = await _unitOfWork.BranchStocks
                .GetWithStockAsync(x => x.Id == dto.BranchStockId);

            if (assignedStock == null)
                return Json(new StockErrorResponse());

            var prevCount = assignedStock.Count;
            assignedStock.Count = dto.Count;

            _unitOfWork.BranchStockLogs.AddLog(assignedStock, User.GetUserId(), prevCount, dto.Count);
            await _unitOfWork.SaveAsync();

            return Json(new StockCountUpdateResponse(assignedStock.Stock.Name, dto.Count));
        }

        /// <summary>
        /// move stocks to another branch
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [AuthorizeRoles(true, UserPermissions.MoveStocks)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<JsonResult> MoveStock([FromBody] MoveStockDto dto)
        {
            var sourceBranchStock = await _unitOfWork.BranchStocks
                .GetOneAsync(x => x.StockId == dto.StockId & x.BranchId == dto.FromBranchId);

            var sourcePrevCount = sourceBranchStock.Count;
            sourceBranchStock.Count = sourceBranchStock.Count - dto.Count;

            var destinationBranchStock = await _unitOfWork.BranchStocks
                .GetOneWithBranchAndStockAsync(x => x.StockId == dto.StockId & x.BranchId == dto.ToBranchId);

            int destinationPrevCount;
            if(destinationBranchStock == null)
            {
                destinationPrevCount = 0;
                // create branch stock if null.
                destinationBranchStock = new BranchStocks()
                {
                    BranchId = dto.ToBranchId,
                    Count = dto.Count,
                    StockId = dto.StockId
                };
                _unitOfWork.BranchStocks.Add(destinationBranchStock);
                await _unitOfWork.SaveAsync();

                // override variable to get data needed for response.
                destinationBranchStock = await _unitOfWork.BranchStocks
                    .GetOneWithBranchAndStockAsync(x => x.Id == destinationBranchStock.Id);
            }
            else
            {
                destinationPrevCount = destinationBranchStock.Count;
                destinationBranchStock.Count = destinationBranchStock.Count + dto.Count;
                await _unitOfWork.SaveAsync();
            }

            var userId = User.GetUserId();
            // create log for source stock
            _unitOfWork.BranchStockLogs
                .AddLog(sourceBranchStock, userId, sourcePrevCount, sourceBranchStock.Count);
            // create log for destination stock
            _unitOfWork.BranchStockLogs
                .AddLog(destinationBranchStock, userId, destinationPrevCount, destinationBranchStock.Count);
            await _unitOfWork.SaveAsync();

            return Json(new StockMovedResponse(
                destinationBranchStock.Branch.Name, 
                destinationBranchStock.Stock.Name, 
                dto.Count,
                sourceBranchStock.Count));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [AuthorizeRoles(true, UserPermissions.ListAndViewStocks)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<JsonResult> GetLogs([FromBody] GetDetailsDto dto)
        {
            var stock = await _unitOfWork.Stocks.FindAsync(x => x.Id == dto.Id);

            if (stock == null)
                return Json(new StockErrorResponse());

            var isAdmin = User.IsInRole(UserPermissions.Admin);
            var isPermitted = User.IsInRole(UserPermissions.ListAndViewStocks);

            var requestedBranchId = dto.BranchId > 0 
                ? dto.BranchId 
                : await _unitOfWork.Users.GetAssignedBranchAsync(User.GetUserId());

            var stockData = await _unitOfWork.BranchStocks.GetLogsAndStockAsync(x => x.BranchId == requestedBranchId && x.StockId == stock.Id);

            if (isPermitted || isAdmin)
            {
                if(isAdmin)
                {
                    // add branch data for admins
                    return Json(new GetLogsResponse()
                    {
                        Branches = await _unitOfWork.Branches.AllAsync(),
                        Payload = stockData,
                        PermitBranchSelect = isAdmin
                    });
                }

                // not admin.
                return Json(new GetLogsResponse()
                {
                    PermitBranchSelect = isAdmin,
                    Payload = stockData
                });
            }
                
            
            return Json(new StockErrorResponse());
        }
    }
}