﻿using AutoMapper;
using Illuminate.Async.DTO.Permissions;
using Illuminate.Async.Responses.Permissions;
using Illuminate.Async.Responses.User;
using Illuminate.Components.Authorization;
using Illuminate.DataAccess.Interfaces;
using Illuminate.DataAccess.Models;
using Illuminate.ViewModels.Permissions;
using Illuminate.ViewModels.User;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Illuminate.Controllers
{
    [AuthorizeRoles(true, UserPermissions.ManagePermissions)]
    public class PermissionsController : Controller
    {
        private readonly IMapper _mapper;

        private readonly IUnitOfWork _unitOfWork;

        private readonly UserManager<User> _userManager;

        public PermissionsController(IMapper mapper,
            IUnitOfWork unitOfWork,
            UserManager<User> userManager)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
            _userManager = userManager;
        }

        /// <summary>
        /// list all users
        /// </summary>
        /// <returns></returns>
        public async Task<IActionResult> Index()
        {
            var collection = await _unitOfWork.Users.ForListingAsync();
            var mappedCollection = _mapper.Map<IEnumerable<User>, List<UserViewModel>>(collection);

            return View(new UserListViewModel
            {
                Users = mappedCollection
            });
        }

        /// <summary>
        /// manage view for setting user permissions
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<IActionResult> Manage(string id)
        {
            var user = await _userManager.FindByIdAsync(id);

            return View(new ManagePermissionsViewModel()
            {
                User = user
            });
        }

        /// <summary>
        /// Get all permissions of current user
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<JsonResult> GetUserPermissions([FromBody] GetUserPermissionsDto dto)
        {
            var user = await _userManager.FindByIdAsync(dto.Id);

            // return error if user is not found
            if (user == null)
                return Json(new UserNotFoundResponse());

            var permissions = typeof(UserPermissions).GetFields();
            List<string> assignedPermissions = (List<string>) await _userManager.GetRolesAsync(user);

            List<UserPermissionDto> permissionsDto = new List<UserPermissionDto>();
            foreach(var permission in permissions)
            {
                // get display attrib data.
                DisplayAttribute displayAttrib = permission.GetCustomAttributes(typeof(DisplayAttribute), true)
                    .Cast<DisplayAttribute>()
                    .First();

                string permissionVal = permission.GetValue(permission).ToString();
                permissionsDto.Add(new UserPermissionDto()
                {
                    Assigned = assignedPermissions.FirstOrDefault(x => x == permissionVal) != null, // if not null then user has permission
                    Permission = permissionVal,
                    PermissionArea = displayAttrib.GroupName,
                    PermissionLabel = displayAttrib.Name,
                    PermissionDesc = displayAttrib.Description,
                    UserId = user.Id
                });
            }

            // order by area and return
            return Json(permissionsDto.OrderBy(x => x.PermissionArea).ToList());
        }

        /// <summary>
        /// Grant/remove permission to user
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<JsonResult> AssignPermission([FromBody] UserPermissionDto dto)
        {
            var user = await _userManager.FindByIdAsync(dto.UserId);
            if (user == null) // ensure that user exists else send not found response
                return Json(new UserNotFoundResponse());

            // check if user is assigned to requested permission
            var isAssigned = await _userManager.IsInRoleAsync(user, dto.Permission);
            if (dto.Assigned)
            {                
                if (!isAssigned)
                    await _userManager.AddToRoleAsync(user, dto.Permission);

                return Json(new PermissionAddedResponse(dto.PermissionLabel));
            }
            else
            {
                if (isAssigned)
                    await _userManager.RemoveFromRoleAsync(user, dto.Permission);

                return Json(new PermissionRemovedResponse(dto.Permission));
            }
        }
    }
}