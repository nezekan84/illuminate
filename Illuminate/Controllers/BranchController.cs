﻿using AutoMapper;
using Illuminate.Async.DTO.Branch;
using Illuminate.Components.Authorization;
using Illuminate.DataAccess.Interfaces;
using Illuminate.DataAccess.Models;
using Illuminate.ViewModels.Branch;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Illuminate.Controllers
{
    [AuthorizeRoles(true, UserPermissions.ManageBranches)]
    public class BranchController : Controller
    {
        private IMapper _mapper;

        private readonly IUnitOfWork _unitOfWork;

        public BranchController(IMapper mapper, 
            IUnitOfWork unitOfWork)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }

        /// <summary>
        /// list all branches
        /// </summary>
        /// <returns></returns>
        public async Task<ViewResult> Index()
        {
            var collection = await _unitOfWork.Branches.AllAsync();
            return View(new ListingViewModel
            {
                Branches = (List<Branches>) collection 
            });
        }

        /// <summary>
        /// create view
        /// </summary>
        /// <returns></returns>
        public ViewResult Create()
        {
            return View(new CreateUpdateBranchViewModel());
        }

        /// <summary>
        /// http post/create function for Create()
        /// </summary>
        /// <param name="viewModel"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> OnCreate(CreateUpdateBranchViewModel viewModel)
        {
            if(!ModelState.IsValid)
                return View("Create", viewModel);

            var model = new Branches();
            _mapper.Map(viewModel, model);
            _unitOfWork.Branches.Add(model);
            await _unitOfWork.SaveAsync();

            return RedirectToAction("Manage", new { id = model.Id });
        }

        /// <summary>
        /// view branch
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<IActionResult> Manage(int id)
        {
            var model = await _unitOfWork.Branches.FindAsync(x => x.Id == id);
            if (model == null)
                return RedirectToAction("Index");

            var viewModel = _mapper.Map<CreateUpdateBranchViewModel>(model);
            return View(viewModel);
        }

        /// <summary>
        /// update function for Manage()
        /// </summary>
        /// <param name="viewModel"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Update(CreateUpdateBranchViewModel viewModel)
        {
            if(!ModelState.IsValid)
                return View("Manage", viewModel);

            var model = await _unitOfWork.Branches.FindAsync(x => x.Id == viewModel.Id);
            _mapper.Map(viewModel, model);
            await _unitOfWork.SaveAsync();

            return RedirectToAction("Manage", new { id = model.Id });
        }

        /// <summary>
        /// set (negate) branch IsActive value
        /// </summary>
        /// <param name="branch"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<RedirectToActionResult> SetActive(Branches branch)
        {
            var model = await _unitOfWork.Branches.GetBranchStateAsync(x => x.Id == branch.Id);
            if (model == null)
                return RedirectToAction("Index");

            model.IsActive = !model.IsActive;
            await _unitOfWork.SaveAsync();

            return RedirectToAction("Index");
        }

        /// <summary>
        /// NOTE: TEMPORARY METHOD FOR LISTING BRANCHES. 
        /// </summary>
        /// <param name="id">except specified branchId</param>
        /// <returns></returns>
        [AuthorizeRoles(true, UserPermissions.MoveStocks)]
        [HttpPost]
        [ValidateAntiForgeryToken]        
        public async Task<JsonResult> GetAll([FromBody] GetAllExceptDto dto)
        {
            return Json(await _unitOfWork.Branches.AllExceptAsync(dto.Id));
        }
    }
}