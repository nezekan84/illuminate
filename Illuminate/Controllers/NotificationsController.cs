﻿using Illuminate.Components;
using Illuminate.Components.Authorization;
using Illuminate.Components.Notification;
using Illuminate.Components.Notification.DTO;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Illuminate.Controllers
{
    [AuthorizeRoles]
    public class NotificationsController : Controller
    {
        private readonly INotificationService _notificationService;

        public NotificationsController(INotificationService notificationService)
        {
            _notificationService = notificationService;
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<JsonResult> Initial()
        {
            var userId = User.GetUserId();

            return Json(await _notificationService.GetInitialData(userId));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<JsonResult> Paginate([FromBody] PaginateDto dto)
        {
            var userId = User.GetUserId();
            var payload = await _notificationService.Paginate(userId, dto.Take, dto.Skip);

            return Json(payload);
        }
    }
}