﻿namespace Illuminate.Async.DTO.Stock
{
    public class ChangeStockCountDto
    {
        public int BranchStockId { get; set; }

        public int Count { get; set; }
    }
}
