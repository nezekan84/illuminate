﻿using Illuminate.DataAccess.Models;
using System.Collections.Generic;

namespace Illuminate.Async.DTO.Stock
{
    public class GetLogsResponse
    {
        public IEnumerable<Branches> Branches { get; set; }

        public BranchStocks Payload { get; set; }

        public bool PermitBranchSelect { get; set; }
    }
}
