﻿namespace Illuminate.Async.DTO.Stock
{
    public class AssignStockDto
    {
        public int BranchStockId { get; set; }

        public int BranchId { get; set; }

        public int StockId { get; set; }

        public bool Remove { get; set; }
    }
}
