﻿namespace Illuminate.Async.DTO.Stock
{
    public class GetDetailsDto
    {
        public int Id { get; set; }

        public int BranchId { get; set; }
    }
}
