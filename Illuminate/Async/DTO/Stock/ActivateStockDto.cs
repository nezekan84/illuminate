﻿namespace Illuminate.Async.DTO.Stock
{
    public class ActivateStockDto
    {
        public int BranchStockId { get; set; }

        public int BranchId { get; set; }

        public int StockId { get; set; }

        public bool Activate { get; set; }
    }
}
