﻿namespace Illuminate.Async.DTO.Stock
{
    public class MoveStockDto
    {
        public int FromBranchId { get; set; }

        public int ToBranchId { get; set; }

        public int StockId { get; set; }

        public int Count { get; set; }
    }
}
