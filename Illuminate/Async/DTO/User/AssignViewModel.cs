﻿namespace Illuminate.Async.DTO.User
{
    public class AssignUserDto
    {
        public string UserId { get; set; }

        public int BranchId { get; set; }

        public bool Remove { get; set; }
    }
}
