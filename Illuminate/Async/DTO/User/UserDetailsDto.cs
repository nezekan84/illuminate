﻿namespace Illuminate.Async.DTO.User
{
    public class UserDetailsDto
    {
        public string Id { get; set; }

        public int BranchId { get; set; }

        public string BranchName { get; set; }

        public bool IsActive { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Email { get; set; }
    }
}
