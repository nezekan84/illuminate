﻿namespace Illuminate.Async.DTO.Permissions
{
    public class UserPermissionDto
    {
        public string Permission { get; set; }

        public string PermissionArea { get; set; }

        public string PermissionLabel { get; set; }

        public string PermissionDesc { get; set; }

        public string UserId { get; set; }

        public bool Assigned { get; set; }
    }
}
