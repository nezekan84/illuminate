﻿namespace Illuminate.Async.DTO.Permissions
{
    public class GetUserPermissionsDto
    {
        public string Id { get; set; }
    }
}
