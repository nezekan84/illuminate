import React from 'react';
import DOM from 'react-dom';
import StockLogs from '../Components/Stocks/StockLogs';
import { ToastContainer } from 'react-toastify';
import 'datatables.net-bs/css/dataTables.bootstrap.min.css';
import 'react-toastify/dist/ReactToastify.css';
import '../Components/Overlay/ControlOverlay.css';

class StockManageView extends React.Component {
    render() {
        return (
            <div className="row">
                <div className="col-sm-12">
                    <StockLogs />
                </div>
                <ToastContainer />
            </div>
        );
    }
}

DOM.render(
    <StockManageView />,
    document.getElementById("stock-manage-container")
);