import React from 'react';
import DOM from 'react-dom';
import { ToastContainer } from 'react-toastify';
import PermissionsContainer from '../Components/Permissions/PermissionsContainer';
import 'react-toastify/dist/ReactToastify.css';
import '../Components/Overlay/ControlOverlay.css';

class PermissionsManagement extends React.Component {
    render() {
        return (
            <div className="row">
                <PermissionsContainer userId={this.props.userId}
                    userFullName={this.props.userFullName}
                    userEmail={this.props.userEmail}
                />
                <ToastContainer />
            </div>
        );
    }
}

var userId = document.getElementById("user-id").value;
var userFullName = document.getElementById("user-fname").value + " " + document.getElementById("user-lname").value;
var userEmail = document.getElementById("user-email").value;

DOM.render(
    <PermissionsManagement userId={userId} userFullName={userFullName} userEmail={userEmail} />,
    document.getElementById("permissions-container")
  
);