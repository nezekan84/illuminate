﻿import React from 'react';
import DOM from 'react-dom';
import UserContainer from '../Components/Users/UserContainer';
import StocksContainer from '../Components/Stocks/StocksContainer';
import { ToastContainer } from 'react-toastify';
import 'datatables.net-bs/css/dataTables.bootstrap.min.css';
import 'react-toastify/dist/ReactToastify.css';
import '../Components/Overlay/ControlOverlay.css';

class BranchContainer extends React.Component {
    render() {
        return (
            <div className="row">
                <div className="col-sm-12">
                    <UserContainer branchId={branchId} />
                </div>
                <div className="col-sm-12">
                    <StocksContainer branchId={branchId} />
                </div>
                <ToastContainer />
            </div>
        );
    }
}

var branchId = document.getElementById("branch-id").value;

DOM.render(
    <BranchContainer branchId={branchId} />,
    document.getElementById("b-manage-container")
);