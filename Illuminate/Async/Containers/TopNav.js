import React from 'react';
import DOM from 'react-dom';
import NotificationsContainer from '../Components/Notifications/NotificationsContainer';

class TopNav extends React.Component {
    render() {
        return (
            <ul className="nav navbar-nav">
                <NotificationsContainer />
            </ul>
        );
    }
}

DOM.render(
    <TopNav />,
    document.getElementById("top-nav-container")
);