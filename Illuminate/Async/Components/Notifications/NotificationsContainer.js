import InfiniteScroll from 'react-infinite-scroll-component';
import React from 'react';

export default class NotificationsContainer extends React.Component {
    constructor(props) {
        super(props);

        this.notifPerPage = 5;

        this.state = {
            enableAnchorClickEvent: true,
            error: null,
            loaded: false,
            loadMore: false,
            notifications: [],
            totalCount: 0,
            unreadCount: 0
        };

        this.fetchNotifications = this.fetchNotifications.bind(this);
        this.onClickAnchor = this.onClickAnchor.bind(this);
        this.renderChilden = this.renderChilden.bind(this);
        this.renderUnreadNotifLabel = this.renderUnreadNotifLabel.bind(this);
    }

    componentDidMount() {
        $.ajax({
            context: this,
            url: "/Notifications/Initial",
            type: "post",
            contentType: "application/json",
            dataType: "json",
            headers: {
                RequestVerificationToken: document.querySelector("input[type='hidden'][name='__RequestVerificationToken']").value
            },
            success: (res) => {
                this.setState({
                    loaded: true,
                    totalCount: res.totalCount,
                    unreadCount: res.unreadCount
                });
            },
            error: (err) => {
                this.setState({
                    error: err
                });
            }
        });
    }

    fetchNotifications() {   
        $.ajax({
            context: this,
            url: "/Notifications/Paginate",
            type: "post",
            data: JSON.stringify({
                take: this.notifPerPage,
                skip: this.state.notifications.length
            }),
            contentType: "application/json",
            dataType: "json",
            headers: {
                RequestVerificationToken: document.querySelector("input[type='hidden'][name='__RequestVerificationToken']").value
            },
            success: (res) => {
                this.setState({
                    error: null,
                    loadMore: res.notifications.length > 0,
                    notifications: this.state.notifications.concat(res.notifications),
                    unreadCount: res.unreadCount
                });
            },
            error: (err) => {
                this.setState({
                    error: err
                });
            }
        })
        .always(() => {
            $(".notif-stat").hide();
        });
    }

    onClickAnchor() {
        if (this.state.loaded && this.state.enableAnchorClickEvent) {
            this.fetchNotifications();
            this.setState({
                enableAnchorClickEvent: false
            });
        }
    }

    renderChilden() {
        if(this.state.notifications.length > 0) {
            return this.state.notifications.map(function(notif) {
                return (
                    <li key={notif.id}>
                        <a href="#">
                            <strong>{notif.id}//{notif.isRead.toString()}</strong><span>{notif.text}</span>
                        </a>
                    </li>
                );
            });
        }
    }

    renderUnreadNotifLabel() {
        var notifLabel = this.state.totalCount > 1 ? "notifications" : "notification";

        return <li className="header">You have {this.state.unreadCount} new {notifLabel}</li>;
    }

    render() {
        return (
            <li className="dropdown notifications-menu" onClick={this.test}>
                <a href="#" className="dropdown-toggle" data-toggle="dropdown" onClick={this.onClickAnchor}>
                    <i className="fa fa-bell-o" />
                    <span className="label label-warning">{this.state.unreadCount}</span>
                </a>
                <ul className="dropdown-menu">
                    {this.renderUnreadNotifLabel()}
                    <li>
                        <InfiniteScroll dataLength={this.state.notifications.length}
                            next={this.fetchNotifications} 
                            hasMore={this.state.loadMore}
                            loader={<span className="notif-stat"><i className="spinner fa fa-spinner fa-spin" /> Please wait...</span>}
                            scrollableTarget="notif-pager"
                        >
                        <ul id="notif-pager" className="menu">
                            {this.renderChilden()}
                        </ul>
                        </InfiniteScroll>
                    </li>
                    <li className="footer"><a href="#">View all</a></li>
                </ul>
            </li>
        );
    }
}