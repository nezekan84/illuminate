﻿import React from 'react';
import classnames from 'classnames';
import { toast } from 'react-toastify';
import InputNumber from 'rc-input-number';
import { Button, Modal } from 'react-bootstrap';
const $ = require('jquery');
$.DataTable = require('datatables.net-bs');

import 'rc-input-number/assets/index.css';

export default class StockRow extends React.Component {
    constructor(props) {
        super(props);

        this.btnDanger = "btn-danger";
        this.btnPrimary = "btn-primary";

        this.bgGreen = "bg-green";
        this.bgRed = "bg-red"; 

        this.lblActivate = "Activate";
        this.lblDeactivate = "Deactivate";

        this.txActive = "Active";
        this.txInactivate = "Inactive";

        this.state = {
            active: props.stock.isActive,
            checked: props.branchId == props.stock.branchId, // true marks that current stock is assigned to branch
            branchStockId: props.stock.branchStockId,
            count: props.stock.count,

            controls: {                
                activeBadgecls: props.stock.isActive ? this.bgGreen : this.bgRed,
                activeLabel: props.stock.isActive ? this.txActive : this.txInactivate,
                btnActivateCls: props.stock.isActive ? this.btnDanger : this.btnPrimary,
                btnActivateLabel: props.stock.isActive ? this.lblDeactivate : this.lblActivate,
                disable: props.branchId != props.stock.branchId, // true marks that the current stock isnt assigned to current branch. due to props.stock.branchId is 0.
                disableMoveBtn: props.stock.count == 0 || props.branchId != props.stock.branchId
            },
            logsModal: {
                logs: [],
                show: false,
                stockName: ""
            },
            moveModal: {
                count: 1,
                disableSubmit: true,
                show: false                
            }
        };

        this.onActivate = this.onActivate.bind(this);
        this.onAssign = this.onAssign.bind(this);
        this.onChangeCount = this.onChangeCount.bind(this);
        this.onSelectBranch = this.onSelectBranch.bind(this);      

        // modal
        this.onHandleMoveCount = this.onHandleMoveCount.bind(this);
        this.onHideModal = this.onHideModal.bind(this);
        this.onHideLogsModal = this.onHideLogsModal.bind(this);
        this.onShowModal = this.onShowModal.bind(this);
        this.onShowLogsModal = this.onShowLogsModal.bind(this);
        this.onSubmitMoveStock = this.onSubmitMoveStock.bind(this);
        this.selectedBranch = 0;
        this.selectedStock = 0;
    }

    componentWillUnmount() {
        if(this.$logTbl !== undefined)
            this.$logTbl.DataTable().destroy();
    }

    onActivate() {
        $.ajax({
            context: this,
            type: "POST",
            url: "/Stock/ActivateStock",
            data: JSON.stringify({
                branchStockId: this.state.branchStockId,
                branchId: this.props.stock.branchId,
                stockId: this.props.stock.stockId,
                activate: !this.state.active
            }),
            contentType: "application/json",
            dataType: "json",
            headers: {
                RequestVerificationToken: document.querySelector("input[type='hidden'][name='__RequestVerificationToken']").value
            },
            beforeSend: function() {
                this.props.overlayTriggers.show();
            },
            error: function() {
                this.triggerError("");
            },
            success: function(res) {
                if(res.error === null) {      
                    this.triggerSuccess(res.message);
    
                    // have to negate since set hasnt updated yet.
                    var controls = this.state.controls;
                    if(!this.state.active) {
                        controls.activeBadgecls = this.bgGreen;
                        controls.activeLabel = this.txActive;
                        controls.btnActivateCls = this.btnDanger;
                        controls.btnActivateLabel = this.lblDeactivate;

                        this.setState({       
                            active: !this.state.active, 
                            controls: controls
                        });
                    } else {
                        controls.activeBadgecls = this.bgRed;
                        controls.activeLabel = this.txInactivate;
                        controls.btnActivateCls = this.btnPrimary;
                        controls.btnActivateLabel = this.lblActivate;

                        this.setState({
                            active: !this.state.active,
                            controls: controls
                        });
                    }
                } else {
                    this.triggerError(res.error);
                }            
            }
        })
        .always(function() {
            this.props.overlayTriggers.hide();
        });
    }

    onAssign() {
        $.ajax({
            context: this,
            type: "POST",
            url: "/Stock/AssignStock",
            data: JSON.stringify({
                branchStockId: this.state.branchStockId,
                branchId: this.props.branchId,
                stockId: this.props.stock.stockId,
                remove: this.state.checked
            }),
            contentType: "application/json",
            dataType: "json",
            headers: {
                RequestVerificationToken: document.querySelector("input[type='hidden'][name='__RequestVerificationToken']").value
            },
            beforeSend: function () {
                this.props.overlayTriggers.show();
            },
            error: function () {
                this.triggerError("");
            },
            success: function (res) {
                if (res.error === null) {
                    this.setState({
                        branchStockId: res.branchStockId,
                        checked: !this.state.checked
                    });

                    this.triggerSuccess(res.message);

                    // negate value to emulate updated state
                    if (!this.state.checked) {
                        var controls = this.state.controls;
                        controls.disable = true;
                        controls.disableMoveBtn = true;
                        controls.activeBadgecls = this.bgRed,
                        controls.activeLabel = this.txInactivate,
                        controls.btnActivateCls = this.btnPrimary,
                        controls.btnActivateLabel = this.lblActivate
                        // disable controls.
                        this.setState({
                            count: 0,
                            controls: controls
                        });
                    } else {
                        var controls = this.state.controls;
                        controls.disable = false,
                        controls.activeBadgecls = this.bgRed,
                        controls.activeLabel = this.txInactivate,
                        controls.btnActivateCls = this.btnPrimary,
                        controls.btnActivateLabel = this.lblActivate
                        // enable controls
                        this.setState({
                            controls: controls
                        });
                    }
                } else {
                    this.triggerError(res.error);
                }
            }
        })
            .always(function () {
                this.props.overlayTriggers.hide();
            });
    }

    onChangeCount(val) {
        var endPoint = val > this.state.count ? "/Stock/AddStock" : "/Stock/PullStock";

        $.ajax({
            context: this,
            type: "POST",
            url: endPoint,
            data: JSON.stringify({
                branchStockId: this.state.branchStockId,
                count: val
            }),
            contentType: "application/json",
            dataType: "json",
            headers: {
                RequestVerificationToken: document.querySelector("input[type='hidden'][name='__RequestVerificationToken']").value
            },
            beforeSend: function() {
                this.props.overlayTriggers.show();
            },
            error: function() {
                this.triggerError("");
            },
            success: function(res) {
                if(res.error === null) {
                    this.setState({
                        count: res.count
                    });

                    this.triggerSuccess(res.message);
                } else {
                    this.triggerError(res.error);
                }
                
                var controls = this.state.controls;
                controls.disableMoveBtn = res.count == 0;
                this.setState({
                    controls: controls
                });
            }
        })
        .always(function() {
            this.props.overlayTriggers.hide();
        });      
    }

    onHandleMoveCount(val) {
        var modalState = this.state.moveModal;
        modalState.count = val;

        this.setState({
            moveModal: modalState
        });
    }

    onHideModal() {
        var modal = this.state.moveModal;
        modal.logs = [];
        modal.show = false;
        modal.stockName = "";

        this.setState({
            moveModal: modal
        });        
    }

    onHideLogsModal() {
        var modal = this.state.logsModal;
        modal.show = false;

        this.setState({
            logsModal: modal
        });

        this.selectedStock = 0;
    }

    onSelectBranch(ev) {
        var branchId = ev.target.value;
        this.selectedBranch = branchId;
        var modal = this.state.moveModal;
        modal.disableSubmit = branchId < 1;

        this.setState({
            moveModal: modal
        });
    }

    onShowModal() {
        var modal = this.state.moveModal;
        modal.show = true;

        this.setState({
            moveModal: modal
        });
    }

    onShowLogsModal() {
        $.ajax({
            context: this,
            type: "POST",
            url: "/Stock/GetLogs",
            contentType: "application/json",
            dataType: "json",
            data: JSON.stringify({
                branchId: this.props.branchId,
                id: this.props.stock.stockId
            }),
            headers: {
                RequestVerificationToken: document.querySelector("input[type='hidden'][name='__RequestVerificationToken']").value
            },
            success: (res) => {
                if (res.error !== undefined) {
                    this.triggerError(res.error);
                    return;
                }

                var modal = this.state.logsModal;

                modal.show = true;
                modal.stockName = res.payload.stock.name;
                modal.logs = res.payload.logs;
                
                this.setState({
                    logsModal: modal
                });

                this.$logTbl = $(this.logTbl);
                this.$logTbl.DataTable({
                    data: res.payload.logs,
                    columns: [{
                            title: "Date",
                            data: "createdOn",
                            width: 250
                        },
                        {
                            title: "User",
                            render: (data, type, row) => {
                                return row.user.firstName + " " + row.user.lastName;
                            }
                        },
                        {
                            title: "Message",
                            data: "message"
                        }
                    ]
                });
            }
        });
    }

    onSubmitMoveStock() {
        $.ajax({
            context: this,
            type: "POST",
            url: "/Stock/MoveStock",
            data: JSON.stringify({
                fromBranchId: this.props.branchId,
                toBranchId: this.selectedBranch,
                stockId: this.props.stock.stockId,
                count: this.state.moveModal.count
            }),
            contentType: "application/json",
            dataType: "json",
            headers: {
                RequestVerificationToken: document.querySelector("input[type='hidden'][name='__RequestVerificationToken']").value
            },
            before: () => {
                // add overlay
            },
            success: (res) => {
                this.triggerSuccess(res.message);

                this.setState({
                    count: res.newCount
                });

                if(res.newCount == 0) {
                    var controls = this.state.controls;
                    controls.disableMoveBtn = true;

                    this.setState({
                        controls: controls
                    });
                }
            },
            error: () => {
                this.triggerError("");
            }
        })
        .always(() => {
            this.onHideModal();
            // reset modal data
            this.selectedBranch = 0;
            var modal = this.state.moveModal;
            modal.disableSubmit = true;
            modal.count = 1;

            this.setState({
                moveModal: modal
            });
        });
    }

    triggerError(err) {
        if(err === "")
            err = "An error has occured. Please try again later.";

        toast.error(err, {
            position: toast.POSITION.BOTTOM_RIGHT
        });        
    }

    triggerSuccess(msg) {
        toast.success(msg, {
            position: toast.POSITION.BOTTOM_RIGHT
        });
    }

    render() {
        return (
            <tr>
                <td>
                    <input type="checkbox" 
                        checked={this.state.checked}
                        onChange={this.onAssign}
                    />
                </td>
                <td>
                    <span className={classnames("badge", this.state.controls.activeBadgecls)}>
                        {this.state.controls.activeLabel}
                    </span>
                </td>
                <td>
                    {this.props.stock.name} 
                </td>
                <td>
                    <InputNumber 
                        min={0}
                        max={32767}
                        value={this.state.count}
                        onChange={this.onChangeCount}
                        disabled={this.state.controls.disable}
                        readOnly={this.state.controls.disable}
                    />
                </td>
                <td>
                    <div className="row">
                        <div className="col-md-4">
                            <button className={classnames("btn", this.state.controls.btnActivateCls)} onClick={this.onActivate} disabled={this.state.controls.disable}>
                            {this.state.controls.btnActivateLabel}
                            </button>
                        </div>
                        <div className="col-md-4">
                            <button className="btn btn-primary" onClick={this.onShowModal} disabled={this.state.controls.disableMoveBtn}>Move</button>
                        </div>
                        <div className="col-md-4">
                            <button className="btn btn-primary" onClick={this.onShowLogsModal} disabled={this.state.controls.disable}>View Logs</button>
                        </div>
                    </div>                         
                    <Modal show={this.state.moveModal.show} onHide={this.onHideModal}>                        
                        <Modal.Header closeButton>
                            <Modal.Title>Move Stock [{this.props.stock.name}]</Modal.Title>
                        </Modal.Header>
                        <Modal.Body>
                            <div className="form-horizontal">
                                <div className="form-group">
                                    <label className="col-md-4">Destination Branch</label>
                                    <div className="col-md-8">
                                        <select className="form-control" defaultValue={0} onChange={this.onSelectBranch}>
                                            <option readOnly>Select</option>
                                            {this.props.branches.map(function(branch) {
                                                    return <option key={branch.id} value={branch.id} >{branch.name}</option>;
                                            }, this)}
                                        </select>
                                    </div>
                                </div>                            
                                <div className="form-group">
                                    <label className="col-md-4">Stocks to move</label>
                                    <div className="col-md-8">
                                        <InputNumber min={1}
                                            max={this.state.count}
                                            defaultValue={this.state.moveModal.count}
                                            value={this.state.moveModal.count} 
                                            onChange={this.onHandleMoveCount}
                                        />
                                    </div>
                                </div>
                            </div>
                        </Modal.Body>
                        <Modal.Footer>
                            <Button onClick={this.onHideModal}>Close</Button>
                            <Button onClick={this.onSubmitMoveStock} disabled={this.state.moveModal.disableSubmit}>Submit</Button>                            
                        </Modal.Footer>
                    </Modal>
                    <Modal show={this.state.logsModal.show} onHide={this.onHideLogsModal} bsSize="large">                        
                        <Modal.Header closeButton>
                            <Modal.Title>Logs for [{this.state.logsModal.stockName}]</Modal.Title>
                        </Modal.Header>
                        <Modal.Body>
                            <div className="box box-primary">
                                <div className="box-body">
                                <table className="table table-hover table-bordered table-striped" ref={logTbl => this.logTbl = logTbl}/>
                                </div>
                            </div>
                        </Modal.Body>
                        <Modal.Footer>
                            <Button onClick={this.onHideLogsModal}>Close</Button>                           
                        </Modal.Footer>
                    </Modal>
                </td>
            </tr>
        );
    }
}