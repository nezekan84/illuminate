﻿import React from 'react';
import StocksTable from './StocksTable';
import { toast } from 'react-toastify';
import { ControlOverlay, ControlOverlayTriggers } from '../Overlay/ControlOverlay';

export default class StocksContainer extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            branches: [],
            stocks: []
        };

        this.overlayId = "for-stocks-tbl";
        this.overlayTriggers = new ControlOverlayTriggers(this.overlayId);
    }

    componentDidMount() {
        var endPoint = "/Stock/ForBranch/{id}".replace("{id}", this.props.branchId);
        fetch(endPoint)
            .then(res => res.json())
            .then(
                (result) => {                    
                    this.setState({
                        stocks: result
                    });
                },
                (err) => {
                    toast.error("Unable to retrieve user list.", {
                        position: toast.POSITION.BOTTOM_RIGHT
                    });
                }
            );

        $.ajax({
            context: this,
            url: "/Branch/GetAll",
            type: "POST",
            contentType: "application/json",
            dataType: "json",
            data: JSON.stringify({
                id: this.props.branchId
            }),
            headers: {
                RequestVerificationToken: document.querySelector("input[type='hidden'][name='__RequestVerificationToken']").value
            },
            success: function (res) {
                this.setState({
                    branches: res,
                    branchInit: true
                });
            },
            error: function () {
                toast.error("Unable to retrieve branch listing.", {
                    position: toast.POSITION.BOTTOM_RIGHT
                });
            }
        });
    }

    render() {
        return (
            <div className="box box-primary">
                <ControlOverlay id={this.overlayId} />
                <div className="box-header">
                    <h3 className="box-title">Stocks</h3>
                    <div className="box-tools pull-right">
                        <button type="button" className="btn btn-box-tool" data-widget="collapse"><i className="fa fa-minus" /></button>
                    </div>
                </div>
                <div className="box-body">
                    <StocksTable
                        branches={this.state.branches}
                        branchId={this.props.branchId}
                        key={Date.now()}
                        overlayTriggers={this.overlayTriggers}
                        stocks={this.state.stocks}
                    />
                </div>
            </div>
        );
    }
}