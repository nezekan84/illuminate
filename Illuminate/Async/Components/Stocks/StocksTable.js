import React from 'react';
import StockRow from './StockRow';

export default class StocksTable extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            branches: [],
            branchInit: false
        };
    }

    renderChildren() {
        if (this.props.stocks.length > 0) {            
            return this.props.stocks.map((stock) => {
                return <StockRow branchId={this.props.branchId} branches={this.props.branches} overlayTriggers={this.props.overlayTriggers} stock={stock} key={stock.branchStockId + stock.count + stock.name} />;
            });
        }
    }

    render() {
        return (
            <div>
                <table id="stock-list" className="table table-hover table-responsive table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>Assigned</th>
                            <th>Active</th>
                            <th>Name</th>
                            <th>Count</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.renderChildren()}
                    </tbody>
                </table>
            </div>
        );
    }
}