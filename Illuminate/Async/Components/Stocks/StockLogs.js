import React from 'react';
import { ControlOverlay, ControlOverlayTriggers } from '../Overlay/ControlOverlay';
import { toast } from 'react-toastify';
import { Button, Modal } from 'react-bootstrap';
const $ = require('jquery');
$.DataTable = require('datatables.net-bs');

export default class StockLogs extends React.Component {
    constructor(props) {
        super(props);       
        this.$ele = null;

        this.state = {
            branches: [],
            permitBranchSelect: false,
            selectBranchModal: {
                disableSelectBtn: true,
                show: false
            }
        };

        this.selectedBranch = 0;

        this.overlayId = "logs-container";
        this.overlayTriggers = new ControlOverlayTriggers(this.overlayId);

        this.getLogs = this.getLogs.bind(this);
        this.onHideSelectBranchModal = this.onHideSelectBranchModal.bind(this);
        this.onSelectBranch = this.onSelectBranch.bind(this);
        this.onShowSelectBranchModal = this.onShowSelectBranchModal.bind(this);
        this.submitSelectedBranch = this.submitSelectedBranch.bind(this);
    }

    componentDidMount() {
        this.getLogs({
            id: document.getElementById("stock-id").value
         });
    }

    componentWillUnmount() {
        this.$ele.DataTable().destroy();
    }

    getLogs(postParams) {
        // check if $ele still contains DataTable instance and destroy it.
        if(this.$ele !== null)
            this.$ele.DataTable().destroy();
        
        $.ajax({
            context: this,
            type: "POST",
            url: "/Stock/GetLogs",
            contentType: "application/json",
            dataType: "json",
            data: JSON.stringify(postParams),
            headers: {
                RequestVerificationToken: document.querySelector("input[type='hidden'][name='__RequestVerificationToken']").value
            },
            beforeSend: ()=> {
                this.overlayTriggers.show();
            },
            success: (res)=> {
                if (res.permitBranchSelect) {
                    this.setState({
                        branches: res.branches,
                        permitBranchSelect: res.permitBranchSelect
                    });
                } else {
                    this.setState({
                        permitBranchSelect: res.permitBranchSelect
                    });
                }

                this.$ele = $(this.ele);
                this.$ele.DataTable({
                    data: res.payload.logs,
                    columns: [{
                            title: "Date",
                            data: "createdOn",
                            width: 250
                        },
                        {
                            title: "User",
                            render: (data, type, row) => {
                                return row.user.firstName + " " + row.user.lastName;
                            },
                            width: 250
                        },
                        {
                            title: "Message",
                            data: "message"
                        }
                    ]
                });
            },
            error: ()=> {
                toast.error("Failed to load logs.", {
                    position: toast.POSITION.BOTTOM_RIGHT
                });
            }
        })
        .always(()=> {
            this.overlayTriggers.hide();
        });
    }

    onHideSelectBranchModal() {
        var modal = this.state.selectBranchModal;
        modal.show = false;

        this.setState({
            selectBranchModal: modal
        });
    }

    onSelectBranch(ev) {
        var branchId = ev.target.value;
        console.log(branchId);
        this.selectedBranch = branchId;

        var modal = this.state.selectBranchModal;
        modal.disableSelectBtn = branchId < 1;

        this.setState({
            selectBranchModal: modal
        });
    }

    onShowSelectBranchModal() {
        var modal = this.state.selectBranchModal;
        modal.show = true;
        
        this.setState({
            selectBranchModal: modal
        });
    }

    renderPermitSelectBtn() {
        if(this.state.permitBranchSelect)
            return <button className="btn btn-primary" onClick={this.onShowSelectBranchModal}>Select Branch</button>;
    }

    submitSelectedBranch() {
        this.getLogs({
            id: document.getElementById("stock-id").value,
            branchId: this.selectedBranch
        });

        this.onHideSelectBranchModal();
        this.selectedBranch = 0;

        var modal = this.state.selectBranchModal;
        modal.disableSelectBtn = true;
        this.setState({
            selectBranchModal: modal
        });
    }

    render() {
        return (
            <div className="box box-primary">                
                <div className="box-header">
                    <h3 className="box-title">Logs</h3>
                    {this.renderPermitSelectBtn()}
                    <div className="box-tools pull-right">
                        <button type="button" className="btn btn-box-tool" data-widget="collapse"><i className="fa fa-minus" /></button>
                    </div>
                </div>
                <div className="box-body">
                    <ControlOverlay id={this.overlayId} />
                    <table className="table table-hover table-bordered table-striped" ref={ele => this.ele = ele}/>
                    <Modal show={this.state.selectBranchModal.show} onHide={this.onHideSelectBranchModal}>                        
                        <Modal.Header closeButton>
                            <Modal.Title>Select Branch</Modal.Title>
                        </Modal.Header>
                        <Modal.Body>
                            <div className="form-horizontal">
                                <div className="form-group">
                                    <label className="col-md-4">Branch</label>
                                    <div className="col-md-8">
                                        <select className="form-control" onChange={this.onSelectBranch} defaultValue={0}>
                                            <option readOnly>Select</option>
                                            {this.state.branches.map(function(branch) {
                                                    return <option key={branch.id} value={branch.id} >{branch.name}</option>;
                                            }, this)}
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </Modal.Body>
                        <Modal.Footer>
                            <Button onClick={this.submitSelectedBranch} disabled={this.state.selectBranchModal.disableSelectBtn}>Select</Button>
                            <Button onClick={this.onHideSelectBranchModal}>Close</Button>                           
                        </Modal.Footer>
                    </Modal>
                </div>
            </div>
        );
    }
}