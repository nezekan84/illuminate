import React from 'react';

const containerCls = "control-overlay";

class ControlOverlay extends React.Component {
    constructor(props) {
        super(props);

        this.containerCls = `${containerCls} bg-light-blue`;        
    }

    render() {
        return (
            <div id={this.props.id} className={this.containerCls}>
                <div className="spinner-container">
                    <i className="spinner fa fa-spinner fa-spin fa-3x" />
                </div>
            </div>
        );
    }
}

class ControlOverlayTriggers {
    constructor(id) {
        this.activeCls = "active";
        this.id = id;
        this.containerCls = `${containerCls}`;      
    }

    ele() {
        return document.querySelector(`#${this.id}.${this.containerCls}`);
    }

    hide() {
        var ele = this.ele();
        
        if(ele.classList.contains(this.containerCls))
            ele.classList.remove(this.activeCls);
    }

    show() {
        var ele = this.ele();
        
        if(!ele.classList.contains(this.activeCls))
            ele.classList.add(this.activeCls);
    }
}

export {
    ControlOverlay,
    ControlOverlayTriggers
};