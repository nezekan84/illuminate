import React from 'react';
import { toast } from 'react-toastify';

export default class PermissionItem extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            checked: props.permission.assigned
        };

        this.onAssignPermission = this.onAssignPermission.bind(this);
    }

    onAssignPermission() {
        var permission = this.props.permission;
        permission.assigned = !this.state.checked; // negate this.

        $.ajax({
            context: this,
            type: 'post',
            url: '/Permissions/AssignPermission',
            data: JSON.stringify(permission),
            contentType: "application/json",
            dataType: "json",            
            headers: {
                RequestVerificationToken: document.querySelector("input[type='hidden'][name='__RequestVerificationToken']").value
            },
            beforeSend: function() {
                this.props.overlayTriggers.show();
            },
            error: function() {
                toast.error("An error has occured. Please try again later", {
                    position: toast.POSITION.BOTTOM_RIGHT
                });
            },
            success: function(response) {
                toast.success(response.message, {
                    position: toast.POSITION.BOTTOM_RIGHT
                });

                this.setState({
                    checked: !this.state.checked
                });
            }
        })
        .always(function() {
            this.props.overlayTriggers.hide();
        });
    }

    render() {
        return (
            <li>
                <input className="permission-cb" type="checkbox" checked={this.state.checked} onChange={this.onAssignPermission} />
                <div className="permission-box">
                    <span className="permission-label">{this.props.permission.permissionLabel}</span>
                    <span className="permission-desc bg-light-blue-active color-palette">{this.props.permission.permissionDesc}</span>
                </div>
            </li>
        );
    }
}