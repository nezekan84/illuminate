import React from 'react';
import PermissionItem from './PermissionItem';

export default class PermissionList extends React.Component {
    constructor(props) {
        super(props);

        this.renderItems = this.renderItems.bind(this);
    }

    renderItems() {
        if(this.props.permissionItems.length > 0) {
            return this.props.permissionItems.map(function(permission) {
                return <PermissionItem key={permission.permissionLabel + permission.assigned.Tostring} overlayTriggers={this.props.overlayTriggers} permission={permission}/>;
            }, this);
        }
    }

    render() {
        return (
            <li>
                <span>{this.props.permissionItems[0].permissionArea}</span>
                <ul>
                    {this.renderItems()}
                </ul>
            </li>
        );
    }
}