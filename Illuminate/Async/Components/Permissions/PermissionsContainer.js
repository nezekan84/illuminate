import React from 'react';
import { toast } from 'react-toastify';
import { ControlOverlay, ControlOverlayTriggers } from '../Overlay/ControlOverlay';
import PermissionList from './PermissionList';
import './styles.css';

export default class PermissionsContainer extends React.Component {
    constructor(props) {
        super(props);

        this.overlayId = "permissions-overlay";
        this.overlayTriggers = new ControlOverlayTriggers(this.overlayId);

        this.state = {
            permissions: []
        };

        this.renderPermissions = this.renderPermissions.bind(this);
    }

    componentDidMount() {
        $.ajax({
            context: this,
            type: "Post",
            url: "/Permissions/GetUserPermissions/",
            data: JSON.stringify({
                id: this.props.userId
            }),
            contentType: "application/json",
            dataType: "json",            
            headers: {
                RequestVerificationToken: document.querySelector("input[type='hidden'][name='__RequestVerificationToken']").value
            },            
            beforeSend: function() {
                this.overlayTriggers.show();
            },
            error: function() {
                toast.error("An error has occured. Please try again later", {
                    position: toast.POSITION.BOTTOM_RIGHT
                });
            },
            success: function(response) {
                this.setState({
                    permissions: response
                });
            }
        })
        .always(function() {
            this.overlayTriggers.hide();
        });
    }

    renderPermissions() {
        var rows = [];

        if(this.state.permissions.length > 0) {
            var x = 0;
            while(x < this.state.permissions.length) {
                // current permission
                var permission = this.state.permissions[x];
                // set current area
                var currentArea = permission.permissionArea;
                var items = [];
                items.push(permission);
                             
                ++x; 
                if(x < this.state.permissions.length) {
                    // loop until currentArea isnt equal next permission currentArea
                    var nextPermission = this.state.permissions[x];
                    while(currentArea === nextPermission.permissionArea) {
                        items.push(nextPermission);
    
                        ++x;
                        nextPermission = this.state.permissions[x]; // move to next permission
                    }
                }
                
                rows.push(<PermissionList key={currentArea} overlayTriggers={this.overlayTriggers} permissionItems={items} />);
            }
        }

        return rows;
    }

    render() {
        return (
            <div className="col-md-6">
                <div className="box box-primary">
                    <div className="box-header">
                        <h3 className="box-title">Manage Permissions for [{this.props.userFullName} - {this.props.userEmail}]</h3>
                    </div>
                    <div className="box-body">
                        <ul>
                            {this.renderPermissions()}
                        </ul>
                        <ControlOverlay id={this.overlayId} />
                    </div>
                </div>
            </div>
        );
    }
}