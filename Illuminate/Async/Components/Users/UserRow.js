﻿import React from 'react';
import classnames from 'classnames';
import { toast } from 'react-toastify';
import 'rc-input-number/assets/index.css';

export default class UserRow extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            checked: props.branchId == props.user.branchId,
            branchName: props.user.branchName
        };
        
        this.handleClick = this.handleClick.bind(this);
    }
    
    handleClick() {   
        $.ajax({
            context: this,
            type: "POST",
            url: "/User/AssignToBranch",
            data: JSON.stringify({
                UserId: this.props.user.id,
                BranchId: this.props.branchId,
                Remove: this.state.checked
            }),
            contentType: "application/json",
            dataType: "json",
            headers: {
                RequestVerificationToken: document.querySelector("input[type='hidden'][name='__RequestVerificationToken']").value
            },
            beforeSend: function() {
                this.props.overlayTriggers.show();
            },
            error: function() {
                toast.error("An error has occured. Please try again later", {
                    position: toast.POSITION.BOTTOM_RIGHT
                });
            },
            success: function(res) {
                if(res.error === null) {
                    this.setState({
                        branchName: res.branch,
                        checked: !this.state.checked
                    });
    
                    toast.success(res.message, {
                        position: toast.POSITION.BOTTOM_RIGHT
                    });
                } else {
                    toast.error(res.error, {
                        position: toast.POSITION.BOTTOM_RIGHT
                    });
                }            
            }
        })
        .always(function() {
            this.props.overlayTriggers.hide();
        });       
    }

    render() {
        var activeBadgecls = this.props.user.isActive ? "bg-green" : "bg-red";
        var activeLabel = this.props.user.isActive ? "Active" : "Inactive";

        return (
            <tr>
                <td>
                    <input type="checkbox" checked={this.state.checked} onChange={this.handleClick} />
                </td>
                <td>{this.state.branchName}</td>
                <td>
                    <span className={classnames("badge", activeBadgecls)}>{activeLabel}</span>
                </td>
                <td>
                    {this.props.user.firstName} 
                </td>
                <td>
                    {this.props.user.lastName}
                </td>
                <td>
                    {this.props.user.email}
                </td>
            </tr>
        );
    }
}