﻿import React from 'react';
import UserTable from './UserTable';
import { toast } from 'react-toastify';
import { ControlOverlay, ControlOverlayTriggers } from '../Overlay/ControlOverlay';

export default class UserContainer extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            users: {}
        };

        this.overlayId = "for-users-tbl";
        this.overlayTriggers = new ControlOverlayTriggers(this.overlayId);
    }

    componentDidMount() {
        fetch("/User/AllUsers")
            .then(res => res.json())
            .then(
                (result) => {                    
                    this.setState({
                        users: result
                    });
                },
                (err) => {
                    toast.error("Unable to retrieve user list.", {
                        position: toast.POSITION.BOTTOM_RIGHT
                    });
                }
            );
    }

    render() {
        return (
            <div className="box box-primary">
                <ControlOverlay id={this.overlayId} />
                <div className="box-header">
                    <h3 className="box-title">Users</h3>
                    <div className="box-tools pull-right">
                        <button type="button" className="btn btn-box-tool" data-widget="collapse"><i className="fa fa-minus" /></button>
                    </div>
                </div>
                <div className="box-body">
                    <UserTable
                        branchId={this.props.branchId}
                        key={Date.now()}
                        overlayTriggers={this.overlayTriggers}
                        users={this.state.users}
                    />
                </div>
            </div>
        );
    }
}