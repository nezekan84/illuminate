﻿import React from 'react';
import UserRow from './UserRow';

export default class UserTable extends React.Component {
    renderChildren() {
        if (this.props.users.length > 0) {            
            return this.props.users.map((user) => {
                return <UserRow branchId={this.props.branchId} overlayTriggers={this.props.overlayTriggers} user={user} key={user.id + user.branchId + user.branchName} />;
            });
        }
    }

    render() {
        return (
            <div>
                <table id="user-list" className="table table-hover table-responsive table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>Assigned</th>
                            <th>Current Branch</th>
                            <th>Active</th>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Email</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.renderChildren()}
                    </tbody>
                </table>
            </div>
        );
    }
}