﻿namespace Illuminate.Async.Responses.Stock
{
    public class StockErrorResponse : BaseResponse
    {
        public StockErrorResponse()
        {
            Error = "An error has occured.";
        }
    }
}
