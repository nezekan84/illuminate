﻿namespace Illuminate.Async.Responses.Stock
{
    public class StockActivateResponse : BaseResponse
    {
        public bool IsActive { get; } = true;

        public StockActivateResponse(string stock)
        {
            Message = $"Stock [{stock}] has been marked as active.";
        }
    }
}
