﻿namespace Illuminate.Async.Responses.Stock
{
    public class StockCountUpdateResponse : BaseResponse
    {
        public int Count { get; }

        public StockCountUpdateResponse(string stock, int count)
        {
            Count = count;
            Message = $"Successfully updated stock [{stock}] count to {count}";
        }
    }
}
