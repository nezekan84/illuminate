﻿namespace Illuminate.Async.Responses.Stock
{
    public class StockRemovedResponse : BaseResponse
    {
        public bool Reload { get; } = true;

        public StockRemovedResponse(string stock)
        {
            Message = $"Successfully removed stock [{stock}].";
        }
    }
}
