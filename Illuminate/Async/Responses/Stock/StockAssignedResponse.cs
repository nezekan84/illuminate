﻿namespace Illuminate.Async.Responses.Stock
{
    public class StockAssignedResponse : BaseResponse
    {
        public int BranchStockId { get; }

        public bool Reload { get; } = true;

        public StockAssignedResponse(int branchStockId, string stock)
        {
            BranchStockId = branchStockId;
            Message = $"Successfully assigned stock [{stock}].";
        }
    }
}
