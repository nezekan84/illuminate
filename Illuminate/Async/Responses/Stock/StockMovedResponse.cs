﻿namespace Illuminate.Async.Responses.Stock
{
    public class StockMovedResponse : BaseResponse
    {
        public int NewCount { get; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="branchName">destination branch name</param>
        /// <param name="stock">stock name</param>
        /// <param name="count"># of stocks moved</param>
        /// <param name="newCount"># of stocks left</param>
        public StockMovedResponse(string branchName, string stock, int count, int newCount)
        {
            NewCount = newCount;
            Message = $"Successfully moved [{stock}] x{count} to [{branchName}]";
        }
    }
}
