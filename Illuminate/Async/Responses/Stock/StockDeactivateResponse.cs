﻿namespace Illuminate.Async.Responses.Stock
{
    public class StockDeactivateResponse : BaseResponse
    {
        public bool IsActive { get; } = false;

        public StockDeactivateResponse(string stock)
        {
            Message = $"Stock{stock} has been marked as inactive";
        }
    }
}
