﻿namespace Illuminate.Async.Responses.Permissions
{
    public class PermissionAddedResponse : BaseResponse
    {
        public PermissionAddedResponse(string permissionLabel)
        {
            Message = $"Successfully granted [{permissionLabel}] to user.";
        }
    }
}
