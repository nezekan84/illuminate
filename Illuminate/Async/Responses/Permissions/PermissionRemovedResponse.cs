﻿namespace Illuminate.Async.Responses.Permissions
{
    public class PermissionRemovedResponse : BaseResponse
    {
        public PermissionRemovedResponse(string permissionLabel)
        {
            Message = $"Successfully removed [{permissionLabel}] permission from user.";
        }
    }
}
