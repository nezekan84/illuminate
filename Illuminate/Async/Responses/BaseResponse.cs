﻿namespace Illuminate.Async.Responses
{
    public class BaseResponse
    {
        public string Error { get; protected set; }

        public string Message { get; protected set; }
    }
}
