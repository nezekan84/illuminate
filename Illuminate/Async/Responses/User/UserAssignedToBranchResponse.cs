﻿namespace Illuminate.Async.Responses.User
{
    public class UserAssignedToBranchResponse : BaseResponse
    {
        public string Branch { get; set; }

        public UserAssignedToBranchResponse(string branch, string firstName, string lastName)
        {
            Branch = branch;
            Message = $"Successfully assigned user [{firstName} {lastName}] to branch [{branch}.";
        }
    }
}
