﻿namespace Illuminate.Async.Responses.User
{
    public class UserNotFoundResponse : BaseResponse
    {
        public UserNotFoundResponse()
        {
            Error = "User not found.";
        }
    }
}
