﻿namespace Illuminate.Async.Responses.User
{
    public class UserRemovedFromBranchResponse : BaseResponse
    {
        public UserRemovedFromBranchResponse(string branch, string firstName, string lastName)
        {
            Message = $"Successfully removed user [{firstName} {lastName}] from branch [{branch}].";
        }
    }
}
