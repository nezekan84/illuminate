﻿namespace Illuminate.Async.Responses.Branch
{
    public class BranchNotFoundResponse : BaseResponse
    {
        public BranchNotFoundResponse()
        {
            Error = "Branch not found";
        }
    }
}
