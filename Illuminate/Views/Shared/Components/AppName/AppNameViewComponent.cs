﻿using Illuminate.Components.Settings;
using Illuminate.DataAccess.Models;
using Illuminate.ViewModels.Components;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace Illuminate.Views.Components.AppName
{
    public class AppNameViewComponent : ViewComponent
    {
        private ISettingsProxy<Settings> _settings;

        public AppNameViewComponent(ISettingsProxy<Settings> settings)
        {
            _settings = settings;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            try
            {
                var appName = await _settings.GetAppSettingAsync(DataAccess.Models.Enums.SettingsAttributes.AppName);

                return View("Default", new AppNameViewModel()
                {
                    NameLarge = appName.Value,
                    NameMini = appName.Value[0]
                });
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}
