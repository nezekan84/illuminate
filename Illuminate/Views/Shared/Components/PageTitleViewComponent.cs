﻿using Illuminate.Components.Settings;
using Illuminate.DataAccess.Models;
using Illuminate.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using System.Threading.Tasks;

namespace Illuminate.Views.Components
{
    public class PageTitleViewComponent : ViewComponent
    {
        private readonly ISettingsProxy<Settings> _settings;

        public PageTitleViewComponent(ISettingsProxy<Settings> settings)
        {
            _settings = settings;
        }

        public async Task<string> InvokeAsync(ViewDataDictionary viewData)
        {
            var appName = await _settings.GetAppSettingAsync(DataAccess.Models.Enums.SettingsAttributes.AppName);

            // this component is created due to the difference between razor pages (mvvm) and views (mvc)
            // this acts like an adapter to create page title
            string pageTitle;
            if (viewData.Model is BaseViewModel)
            {
                BaseViewModel viewModel = (BaseViewModel)viewData.Model;
                pageTitle = viewModel.PageTitle;
            }
            else
            {
                pageTitle = viewData["PageTitle"].ToString();
            }

            return $"{pageTitle} | {appName.Value}";
        }
    }
}
