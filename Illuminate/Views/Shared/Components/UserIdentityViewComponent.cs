﻿using Illuminate.DataAccess.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using System.Threading.Tasks;

namespace Illuminate.Views.Components
{
    public class UserIdentityViewComponent
    {
        private readonly IHttpContextAccessor _httpContext;

        private readonly UserManager<User> _userManager;

        public UserIdentityViewComponent(IHttpContextAccessor httpContext, UserManager<User> userManager)
        {
            _httpContext = httpContext;
            _userManager = userManager;
        }

        public async Task<string> InvokeAsync()
        {
            var user = await _userManager.GetUserAsync(_httpContext.HttpContext.User);

            if (string.IsNullOrEmpty(user.FirstName))
                return user.Email;

            return $"{user.FirstName} {user.LastName}";
        }
    }
}
