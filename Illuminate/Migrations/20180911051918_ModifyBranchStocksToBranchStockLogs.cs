﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Illuminate.Migrations
{
    public partial class ModifyBranchStocksToBranchStockLogs : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "StockLogs");

            migrationBuilder.AlterColumn<int>(
                name: "Count",
                table: "BranchStocks",
                nullable: false,
                oldClrType: typeof(short));

            migrationBuilder.CreateTable(
                name: "BranchStockLogs",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    BranchStockId = table.Column<int>(nullable: false),
                    UserId = table.Column<string>(nullable: true),
                    PreviousIsActive = table.Column<bool>(nullable: false),
                    NewIsActive = table.Column<bool>(nullable: false),
                    PreviousCount = table.Column<int>(nullable: false),
                    NewCount = table.Column<int>(nullable: false),
                    Message = table.Column<string>(nullable: true),
                    CreatedOn = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BranchStockLogs", x => x.Id);
                    table.ForeignKey(
                        name: "FK_BranchStockLogs_BranchStocks_BranchStockId",
                        column: x => x.BranchStockId,
                        principalTable: "BranchStocks",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_BranchStockLogs_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_BranchStockLogs_BranchStockId",
                table: "BranchStockLogs",
                column: "BranchStockId");

            migrationBuilder.CreateIndex(
                name: "IX_BranchStockLogs_UserId",
                table: "BranchStockLogs",
                column: "UserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "BranchStockLogs");

            migrationBuilder.AlterColumn<short>(
                name: "Count",
                table: "BranchStocks",
                nullable: false,
                oldClrType: typeof(int));

            migrationBuilder.CreateTable(
                name: "StockLogs",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    BranchStockId = table.Column<int>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    Message = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StockLogs", x => x.Id);
                    table.ForeignKey(
                        name: "FK_StockLogs_BranchStocks_BranchStockId",
                        column: x => x.BranchStockId,
                        principalTable: "BranchStocks",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_StockLogs_BranchStockId",
                table: "StockLogs",
                column: "BranchStockId");
        }
    }
}
