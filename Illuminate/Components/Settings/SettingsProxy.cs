﻿using Illuminate.DataAccess.Interfaces;
using Illuminate.DataAccess.Models.Enums;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using SettingsModel = Illuminate.DataAccess.Models.Settings;

namespace Illuminate.Components.Settings
{
    public class SettingsProxy : ISettingsProxy<SettingsModel>
    {
        /// <summary>
        /// access this using GetAppSettingAsync().
        /// all created and saved app settings (during the session) must be added here.
        /// </summary>
        private readonly List<SettingsModel> _appSettings = new List<SettingsModel>();

        private readonly IHttpContextAccessor _httpContext;

        private readonly bool _isAuthenticated;

        private readonly IUnitOfWork _unitOfWork;

        /// <summary>
        /// access this using GetUserSettingAsync().
        /// all created and saved user settings (during the request) must be added here.
        /// </summary>
        private readonly List<SettingsModel> _userSettings = new List<SettingsModel>();

        public SettingsProxy(IHttpContextAccessor httpContext,
            IUnitOfWork unitOfWork)
        {
            _httpContext = httpContext;
            _isAuthenticated = httpContext.HttpContext.User.Identity.IsAuthenticated;
            _unitOfWork = unitOfWork;
        }

        public async Task<SettingsModel> CreateOrUpdateAppSettingAsync(SettingsAttributes attribute, string value)
        {
            return await CreateOrUpdateAppSettingInternal(attribute, value, false);
        }

        public async Task<SettingsModel> CreateOrUpdateAppSettingAsync(SettingsAttributes attribute, string value, bool autoload)
        {
            return await CreateOrUpdateAppSettingInternal(attribute, value, autoload);
        }

        public async Task<SettingsModel> CreateOrUpdateUserSettingAsync(SettingsAttributes attribute, string value, string userId)
        {
            return await CreateOrUpdateUserSettingInternal(attribute, value, false, userId);
        }

        public async Task<SettingsModel> CreateOrUpdateUserSettingAsync(SettingsAttributes attribute, string value, string userId, bool autoload)
        {
            return await CreateOrUpdateUserSettingInternal(attribute, value, autoload, userId);
        }

        public async Task<SettingsModel> GetAppSettingAsync(SettingsAttributes attribute)
        {
            // get all auto load
            if (!_appSettings.Any())
            {
                var collection = await _unitOfWork.Settings.LoadAppAutoSettingsAsync();

                if (collection.Any())
                    _appSettings.AddRange(collection);
            }

            return _appSettings
                .FirstOrDefault(setting => setting.Entity == SettingsEntities.App && setting.Attribute == attribute);
        }

        public async Task<SettingsModel> GetUserSettingAsync(SettingsAttributes attribute)
        {
            if (!_isAuthenticated)
                throw new UserNotAuthenticatedException();

            var userId = _httpContext.HttpContext.User.GetUserId();

            // get all auto load
            if (!_userSettings.Any())
            {                
                var collection = await _unitOfWork.Settings.LoadUserAutos(userId);

                if (collection.Any())
                    _userSettings.AddRange(collection);
            }

            return _userSettings
                .FirstOrDefault(x => x.Entity == SettingsEntities.User && x.Attribute == attribute && x.UserId == userId);
        }

        /// <summary>
        /// create/update handler for CreateOrUpdateAppSettingAsync()
        /// </summary>
        /// <param name="attribute"></param>
        /// <param name="value"></param>
        /// <param name="autoload"></param>
        /// <returns></returns>
        private async Task<SettingsModel> CreateOrUpdateAppSettingInternal(SettingsAttributes attribute, string value, bool autoload)
        {
            SettingsModel settings;

            settings = await GetAppSettingAsync(attribute);

            // if null then search for previously loaded app settings
            if (settings == null)
                settings = await Search(x => x.Entity == SettingsEntities.App && x.Attribute == attribute);

            if(settings == null)
            {
                // it still not found then create it
                settings = await CreateAppSetting(attribute, value, autoload);
            }
            else
            {
                // update found setting
                settings.Autoload = autoload;
                settings.Value = value;
                await _unitOfWork.SaveAsync();
            }

            return settings;
        }

        /// <summary>
        /// create/update handler for CreateOrUpdateUserSettingAsync()
        /// </summary>
        /// <param name="attribute"></param>
        /// <param name="value"></param>
        /// <param name="autoload"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        private async Task<SettingsModel> CreateOrUpdateUserSettingInternal(SettingsAttributes attribute, string value, bool autoload, string userId)
        {
            SettingsModel settings;

            settings = await GetAppSettingAsync(attribute);

            // if null then search for previously loaded app settings
            if (settings == null)
                settings = await Search(x => x.Entity == SettingsEntities.User && x.Attribute == attribute && x.UserId == userId);

            if (settings == null)
            {
                // it still not found then create it
                settings = await CreateUserSetting(attribute, value, autoload, userId);
            }
            else
            {
                // update found setting
                settings.Autoload = autoload;
                settings.Value = value;
                await _unitOfWork.SaveAsync();
            }

            return settings;
        }

        /// <summary>
        /// create new app setting
        /// </summary>
        /// <param name="attribute"></param>
        /// <param name="value"></param>
        /// <param name="autoload"></param>
        /// <returns></returns>
        private async Task<SettingsModel> CreateAppSetting(SettingsAttributes attribute, string value, bool autoload)
        {
            var setting = new SettingsModel()
            {
                Autoload = autoload,
                Entity = SettingsEntities.App,
                Attribute = attribute,
                Value = value
            };

            await SaveModel(setting);
            return setting;
        }

        /// <summary>
        /// create new user setting
        /// </summary>
        /// <param name="attribute"></param>
        /// <param name="value"></param>
        /// <param name="autoload"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        private async Task<SettingsModel> CreateUserSetting(SettingsAttributes attribute, string value, bool autoload, string userId)
        {
            var setting = new SettingsModel()
            {
                Autoload = autoload,
                UserId = userId,
                Entity = SettingsEntities.App,
                Attribute = attribute,
                Value = value
            };

            await SaveModel(setting);
            return setting;
        }

        /// <summary>
        /// Push model to list based on Entity property
        /// </summary>
        /// <param name="model"></param>
        private void PushToCollection(SettingsModel model)
        {
            if (model == null)
                return;

            // 'cache' saved model
            switch (model.Entity)
            {
                case SettingsEntities.App:
                    _appSettings.Add(model);
                    break;
                case SettingsEntities.User:
                    _userSettings.Add(model);
                    break;
            }
        }

        /// <summary>
        /// save setting.  if found then record will be 'cached'
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        private async Task SaveModel(SettingsModel model)
        {
            _unitOfWork.Settings.Add(model);

            try
            {
                await _unitOfWork.SaveAsync();
            }
            catch (Exception e)
            {
                throw e;
            }

            PushToCollection(model);
        }

        /// <summary>
        /// Search for a specific setting
        /// </summary>
        /// <param name="expression"></param>
        /// <returns></returns>
        private async Task<SettingsModel> Search(Expression<Func<SettingsModel, bool>> expression)
        {
            var setting = await _unitOfWork.Settings.FindAsync(expression);

            PushToCollection(setting);
            return setting;
        }
    }
}
