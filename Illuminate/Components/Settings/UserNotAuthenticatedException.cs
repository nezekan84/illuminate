﻿using System;

namespace Illuminate.Components.Settings
{
    public class UserNotAuthenticatedException : Exception
    {
        public UserNotAuthenticatedException() : base("User not authenticated.")
        {

        }
    }
}
