﻿using Illuminate.DataAccess.Models.Enums;
using System.Threading.Tasks;
using SettingsModel = Illuminate.DataAccess.Models.Settings;

namespace Illuminate.Components.Settings
{
    public interface ISettingsProxy<TEntity> where TEntity : class
    {
        /// <summary>
        /// Create or update app setting. autoload = false.
        /// </summary>
        /// <param name="attribute"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        Task<SettingsModel> CreateOrUpdateAppSettingAsync(SettingsAttributes attribute, string value);

        /// <summary>
        /// Create or update app setting
        /// </summary>
        /// <param name="attribute"></param>
        /// <param name="value"></param>
        /// <param name="autoload"></param>
        /// <returns></returns>
        Task<SettingsModel> CreateOrUpdateAppSettingAsync(SettingsAttributes attribute, string value, bool autoload);

        /// <summary>
        /// Create or update user setting. autoload = false.
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="attribute"></param>
        /// <param name="value"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        Task<SettingsModel> CreateOrUpdateUserSettingAsync(SettingsAttributes attribute, string value, string userId);
        
        /// <summary>
        /// Create or update user setting
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="attribute"></param>
        /// <param name="value"></param>
        /// <param name="userId"></param>
        /// <param name="autoload"></param>
        /// <returns></returns>
        Task<SettingsModel> CreateOrUpdateUserSettingAsync(SettingsAttributes attribute, string value, string userId, bool autoload);

        /// <summary>
        /// Get app setting
        /// </summary>
        /// <param name="attribute"></param>
        /// <returns></returns>
        Task<SettingsModel> GetAppSettingAsync(SettingsAttributes attribute);

        /// <summary>
        /// Get user setting
        /// </summary>
        /// <param name="attribute"></param>
        /// <returns></returns>
        Task<SettingsModel> GetUserSettingAsync(SettingsAttributes attribute);
    }
}
