﻿using Microsoft.Extensions.DependencyInjection;
using SettingsModel = Illuminate.DataAccess.Models.Settings;

namespace Illuminate.Components.Settings
{
    public static class SettingsProvider
    {
        public static IServiceCollection AddAppSettings(this IServiceCollection services)
        {
            services.AddScoped<ISettingsProxy<SettingsModel>, SettingsProxy>();
            return services;
        }
    }
}
