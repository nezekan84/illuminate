﻿using System.Threading.Tasks;
using Illuminate.Components.Notification.Commands;
using Illuminate.Components.Notification.DTO;

namespace Illuminate.Components.Notification
{
    public class NotificationService : INotificationService
    {
        private readonly INotificationEngine _notifEngine;

        public NotificationService(
            INotificationEngine notifEngine)
        {
            _notifEngine = notifEngine;
        }

        public async Task ActivateStockNotification(string actorId, int entityId, int targetBranchId)
        {            
            var notif = new ActivateStockNotifCommand(_notifEngine)
            {
                ActorId = actorId,
                EntityId = entityId,
                TargetBranchId = targetBranchId
            };
            await notif.ExecuteAsync();
        }

        public void Dispose()
        {
            _notifEngine.Dispose();
        }

        public async Task<InitialDataDto> GetInitialData(string userId)
        {
            var command = new SetInitialDataCommand(_notifEngine)
            {
                UserId = userId
            };
            await command.ExecuteAsync();

            return command.Result();
        }

        public async Task<PaginatedNotifications> Paginate(string userId, int take, int skip)
        {
            var command = new PaginateCommand(_notifEngine)
            {
                UserId = userId,
                Take = take,
                Skip = skip
            };
            await command.ExecuteAsync();

            return command.Result();
        }
    }
}
