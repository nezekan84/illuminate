﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;

namespace Illuminate.Components.Notification
{
    public static class NotificationProvider
    {
        /// <summary>
        /// Register notification services
        /// </summary>
        /// <param name="services"></param>
        /// <returns></returns>
        public static IServiceCollection AddNotifications(this IServiceCollection services)
        {
            services.AddScoped<INotificationEngine, NotificationEngine>();
            services.AddScoped<INotificationService, NotificationService>();
            services.AddTransient<NotificationMiddleware>();

            return services;
        }

        /// <summary>
        /// Register notifation middleware
        /// </summary>
        /// <param name="appBuilder"></param>
        /// <returns></returns>
        public static IApplicationBuilder UseNotifications(this IApplicationBuilder appBuilder)
        {
            appBuilder.UseMiddleware<NotificationMiddleware>();

            return appBuilder;
        }
    }
}
