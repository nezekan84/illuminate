﻿using Illuminate.DataAccess.Models;
using Illuminate.DataAccess.Models.Enums;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Illuminate.Components.Notification
{
    public interface INotificationEngine : IDisposable
    {
        /// <summary>
        /// Create parent entity for notification
        /// </summary>
        /// <param name="actorId"></param>
        /// <param name="typeId"></param>
        /// <param name="entityId"></param>
        /// <returns></returns>
        Task<int> CreateParentAsync(string actorId, EntityTypes typeId, int entityId);

        /// <summary>
        /// Gets number of user notif
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        Task<int> CountAll(string userId);

        /// <summary>
        /// Gets number of unread notifs
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        Task<int> CountUnread(string userId);

        /// <summary>
        /// Gets query string param
        /// </summary>
        /// <returns></returns>
        string GetQueryStringParam();

        /// <summary>
        /// Gets users from selected branmch
        /// </summary>
        /// <param name="branchId"></param>
        /// <param name="exceptCurrentUserId"></param>
        /// <returns></returns>
        Task<IEnumerable<string>> GetUsersFromBranch(int branchId, string exceptCurrentUserId);

        /// <summary>
        /// Notifies target user.
        /// </summary>
        /// <returns></returns>
        Task NotifyAsync(int parentId, string targetUserId);

        /// <summary>
        /// Get notifications
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="take"></param>
        /// <param name="skip"></param>
        /// <returns></returns>
        Task<IEnumerable<Notifications>> PaginateNotifications(string userId, int take, int skip);

        /// <summary>
        /// Marks notification as read
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="notifId"></param>
        /// <returns></returns>
        Task SetIsRead(string userId, int notifId);
    }
}
