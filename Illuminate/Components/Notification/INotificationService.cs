﻿using Illuminate.Components.Notification.DTO;
using System;
using System.Threading.Tasks;

namespace Illuminate.Components.Notification
{
    public interface INotificationService : IDisposable
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="actorId"></param>
        /// <param name="entityId"></param>
        /// <param name="targetBranchId"></param>
        /// <returns></returns>
        Task ActivateStockNotification(string actorId, int entityId, int targetBranchId);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        Task<InitialDataDto> GetInitialData(string userId);

        /// <summary>
        /// Paginate notifications
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="take"></param>
        /// <param name="skip"></param>
        /// <returns></returns>
        Task<PaginatedNotifications> Paginate(string userId, int take, int skip);
    }
}
