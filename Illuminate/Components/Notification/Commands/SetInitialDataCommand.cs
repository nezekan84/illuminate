﻿using Illuminate.Components.Notification.DTO;
using System.Threading.Tasks;

namespace Illuminate.Components.Notification.Commands
{
    public class SetInitialDataCommand : BaseCommand<InitialDataDto>
    {
        public int TotalCount { get; set; }

        public int UnreadCount { get; private set; }

        public string UserId { get; set; }

        public SetInitialDataCommand(INotificationEngine notificationEngine) : base(notificationEngine)
        {
        }

        public async override Task ExecuteAsync()
        {
            TotalCount = await Engine.CountAll(UserId);
            UnreadCount = await Engine.CountUnread(UserId);
        }

        public override InitialDataDto Result()
        {
            return new InitialDataDto {
                TotalCount = TotalCount,
                UnreadCount = UnreadCount
            };
        }
    }
}
