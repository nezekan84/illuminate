﻿using System.Threading.Tasks;

namespace Illuminate.Components.Notification.Commands
{
    public abstract class BaseCommand<TResult> : IBaseCommand<TResult>
    {
        public INotificationEngine Engine { get; }

        public BaseCommand(INotificationEngine notificationEngine)
        {
            Engine = notificationEngine;
        }
        
        public abstract Task ExecuteAsync();

        public abstract TResult Result();
    }
}
