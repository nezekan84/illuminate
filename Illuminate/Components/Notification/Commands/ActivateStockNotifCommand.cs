﻿using Illuminate.DataAccess.Models.Enums;
using System;
using System.Threading.Tasks;

namespace Illuminate.Components.Notification.Commands
{
    public class ActivateStockNotifCommand : BaseCommand<bool>
    {
        public ActivateStockNotifCommand(INotificationEngine notificationEngine) : base(notificationEngine)
        {
        }

        public string ActorId { get; set; }

        public int EntityId { get; set; }

        public EntityTypes EntityType { get; } = EntityTypes.StockActivate;

        public int TargetBranchId { get; set; }

        public override async Task ExecuteAsync()
        {
            var parentId = 0;
            try
            {
                parentId = await Engine.CreateParentAsync(ActorId, EntityType, EntityId);
            }
            catch (Exception e)
            {
                throw e;
            }

            var targetUsers = await Engine.GetUsersFromBranch(TargetBranchId, ActorId);
            foreach(var user in targetUsers)
            {
                await Engine.NotifyAsync(parentId, user);
            }
        }

        public override bool Result() => true;
    }
}
