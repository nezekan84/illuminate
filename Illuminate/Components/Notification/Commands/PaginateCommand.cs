﻿using Illuminate.Components.Notification.DTO;
using Illuminate.Components.Notification.EntityTypesDecorator;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Illuminate.Components.Notification.Commands
{
    public class PaginateCommand : BaseCommand<PaginatedNotifications>
    {
        public List<NotificationItem> Notifications { get; } = new List<NotificationItem>();

        public int Skip { get; set; }

        public int Take { get; set; }

        public string UserId { get; set; }
        
        private int _unreadCount;

        public PaginateCommand(INotificationEngine notificationEngine) : base(notificationEngine)
        {
        }

        public override async Task ExecuteAsync()
        {
            var notifs = await Engine.PaginateNotifications(UserId, Take, Skip);
            Notifications.AddRange(notifs.Select(x => new NotificationItem
            {
                Date = x.NotificationObject.CreatedOn,
                EntityId = x.NotificationObject.EntityId,
                Id = x.Id,
                IsRead = x.IsRead,
                Placeholders = x.NotificationObject.EntityType.Get().TextPlaceholders,
                PlaceholderValues = new List<string>(),
                Text = x.NotificationObject.EntityType.Get().Text
            }));

            _unreadCount = await Engine.CountUnread(UserId);
        }

        public override PaginatedNotifications Result()
        {
            return new PaginatedNotifications
            {
                Notifications = Notifications,
                UnreadCount = _unreadCount
            };
        }
    }
}
