﻿using System.Threading.Tasks;

namespace Illuminate.Components.Notification.Commands
{
    public interface IBaseCommand<TResult>
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        Task ExecuteAsync();

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        TResult Result();
    }
}
