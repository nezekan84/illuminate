﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Illuminate.DataAccess.Interfaces;
using Illuminate.DataAccess.Models;
using Illuminate.DataAccess.Models.Enums;

namespace Illuminate.Components.Notification
{
    public class NotificationEngine : INotificationEngine
    {
        private readonly IUnitOfWork _unitOfWork;

        public NotificationEngine(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<int> CreateParentAsync(string actorId, EntityTypes typeId, int entityId)
        {
            var parent = new NotificationObjects
            {
                ActorId = actorId,
                EntityType = typeId,
                EntityId = entityId,
                CreatedOn = DateTime.Now
            };

            _unitOfWork.NotificationObjects.Add(parent);
            await _unitOfWork.SaveAsync();
            return parent.Id;
        }

        public async Task<int> CountAll(string userId)
        {
            return await _unitOfWork.Notifications.CountAllAsync(userId);
        }

        public async Task<int> CountUnread(string userId)
        {
            return await _unitOfWork.Notifications.CountUnreadAsync(userId);
        }

        public void Dispose()
        {
            _unitOfWork.Dispose();
        }

        public async Task<IEnumerable<string>> GetUsersFromBranch(int branchId, string exceptCurrentUserId)
        {
            return await _unitOfWork.Users.GetUsersFromBranchExceptAsync(branchId, exceptCurrentUserId);
        }

        public string GetQueryStringParam()
        {
            return "notif_id";
        }

        public async Task NotifyAsync(int parentId, string targetUserId)
        {
            _unitOfWork.Notifications.Add(new Notifications
            {
                NotificationObjectId = parentId,
                NotifierId = targetUserId,
                IsRead = false
            });
            await _unitOfWork.SaveAsync();
        }

        public async Task<IEnumerable<Notifications>> PaginateNotifications(string userId, int take, int skip)
        {
            return await _unitOfWork.Notifications.TakeAsync(userId, take, skip);
        }

        public async Task SetIsRead(string userId, int notifId)
        {
            var notif = await _unitOfWork.Notifications
                .FindAsync(x => x.NotifierId == userId && x.Id == notifId);
            notif.IsRead = true;
            await _unitOfWork.SaveAsync();
        }
    }
}
