﻿using Illuminate.DataAccess.Models.Enums;
using System.Collections.Generic;

namespace Illuminate.Components.Notification.EntityTypesDecorator
{
    public class EntityType
    {
        public EntityTypes Entity { get; set; }

        public string Text { get; set; }

        public List<string> TextPlaceholders { get; } = new List<string>();

        public string UrlFormat { get; set; }

        public EntityType(string[] placeholders)
        {
            TextPlaceholders.AddRange(placeholders);
        }
    }
}
