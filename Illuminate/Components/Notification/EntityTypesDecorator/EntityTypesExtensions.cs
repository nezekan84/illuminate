﻿using Illuminate.DataAccess.Models.Enums;

namespace Illuminate.Components.Notification.EntityTypesDecorator
{
    public static class EntityTypesExtensions
    {
        /// <summary>
        /// Gets associated data
        /// </summary>
        /// <param name="entityTypes"></param>
        /// <returns></returns>
        public static EntityType Get(this EntityTypes entityTypes)
        {
            switch(entityTypes)
            {
                case EntityTypes.StockActivate:
                    return new EntityType(new string[] { "{Stock}" })
                    {
                        Entity = EntityTypes.StockActivate,
                        Text = "{Stock} has been added.",
                        UrlFormat = "#"
                    };
                case EntityTypes.StockRemove:
                    return new EntityType(new string[] { "{Stock}" })
                    {
                        Entity = EntityTypes.StockRemove,
                        Text = "{Stock} has been removed.",
                        UrlFormat = "#"
                    };
                case EntityTypes.StockCountUp:
                    return new EntityType(new string[] { "{Stock}", "{Count}" })
                    {
                        Entity = EntityTypes.StockCountUp,
                        Text = "Added {Count} units to {Stock}.",
                        UrlFormat = "#"
                    };
                case EntityTypes.StockCountDown:
                    return new EntityType(new string[] { "{Stock}", "{Count}" })
                    {
                        Entity = EntityTypes.StockCountDown,
                        Text = "Removed {Count} units from {Stock}.",
                        UrlFormat = "#"
                    };
                case EntityTypes.BranchUserAdd:
                    return new EntityType(new string[] { "{Branch}", "{Name}" })
                    {
                        Entity = EntityTypes.BranchUserAdd,
                        Text = "{Name} has been assigned to {Branch}.",
                        UrlFormat = "#"
                    };
                case EntityTypes.BranchUserRemove:
                    return new EntityType(new string[] { "{Branch}", "{Name}" })
                    {
                        Entity = EntityTypes.BranchUserRemove,
                        Text = "{Name} has been removed from {Branch}.",
                        UrlFormat = "#"
                    };
                default:
                    throw new EntityNotDefinedException(entityTypes);
            }
        }
    }
}
