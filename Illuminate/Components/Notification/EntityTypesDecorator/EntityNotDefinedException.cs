﻿using Illuminate.DataAccess.Models.Enums;
using System;

namespace Illuminate.Components.Notification.EntityTypesDecorator
{
    public class EntityNotDefinedException : Exception
    {
        public EntityNotDefinedException(EntityTypes entityType) : 
            base($"EntityTypes value {entityType} is undefined.")
        {
        }
    }
}
