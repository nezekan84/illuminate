﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Primitives;

namespace Illuminate.Components.Notification
{
    public class NotificationMiddleware : IMiddleware
    {
        private readonly INotificationEngine _notificationEngine;

        public NotificationMiddleware(INotificationEngine notificationEngine)
        {
            _notificationEngine = notificationEngine;
        }

        public async Task InvokeAsync(HttpContext context, RequestDelegate next)
        {
            var notifQueryStr = _notificationEngine.GetQueryStringParam();

            // get notif id from query string
            StringValues notifIdStr;
            context.Request.Query.TryGetValue(notifQueryStr, out notifIdStr);

            var notifId = 0;
            int.TryParse(notifIdStr, out notifId);

            if(notifId > 0)
            {
                try
                {
                    // mark as read
                    await _notificationEngine.SetIsRead(context.User.GetUserId(), notifId);
                }
                catch (Exception e)
                {            
                    // let app run.
                }
            }

            await next(context);
        }
    }
}
