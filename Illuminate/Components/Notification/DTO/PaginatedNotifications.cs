﻿using System.Collections.Generic;

namespace Illuminate.Components.Notification.DTO
{
    public class PaginatedNotifications
    {
        public List<NotificationItem> Notifications { get; set; }

        public int UnreadCount { get; set; }
    }
}
