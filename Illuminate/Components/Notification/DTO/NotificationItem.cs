﻿using System;
using System.Collections.Generic;

namespace Illuminate.Components.Notification.DTO
{
    public class NotificationItem
    {
        private DateTime _date;

        public DateTime Date {
            get => new DateTime(_date.Year, _date.Month, _date.Day);
            set => _date = value;
        }

        public string DateFormat { get; } = "MMMM d, yyyy";

        public string DateString => Date.ToString(DateFormat);

        public int EntityId { get; set; }

        public int Id { get; set; }

        public bool IsRead { get; set; }

        public List<string> Placeholders { get; set; }

        public List<string> PlaceholderValues { get; set; }

        public string Text { get; set; }
                
        public string Url { get; set; }
    }
}
