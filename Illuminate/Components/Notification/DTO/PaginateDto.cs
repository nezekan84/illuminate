﻿namespace Illuminate.Components.Notification.DTO
{
    public class PaginateDto
    {
        public int Take { get; set; }

        public int Skip { get; set; }
    }
}
