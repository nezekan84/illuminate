﻿namespace Illuminate.Components.Notification.DTO
{
    public class InitialDataDto
    {
        public int TotalCount { get; set; }

        public int UnreadCount { get; set; }
    }
}
