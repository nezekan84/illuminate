﻿using System.Security.Claims;

namespace Illuminate.Components
{
    public static class ClaimsPrincipalExtensions
    {
        /// <summary>
        /// Get user id
        /// </summary>
        /// <param name="claimsPrincipal"></param>
        /// <returns></returns>
        public static string GetUserId(this ClaimsPrincipal claimsPrincipal)
        {
            return claimsPrincipal.FindFirst(ClaimTypes.NameIdentifier)?.Value;
        }
    }
}
