﻿using System.Threading.Tasks;
using Illuminate.DataAccess.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;

namespace Illuminate.Components.Authorization
{
    public class ShouldBeActiveHandler : AuthorizationHandler<ShouldBeActive>
    {
        private readonly UserManager<User> _userManager;

        public ShouldBeActiveHandler(UserManager<User> userManager)
        {
            _userManager = userManager;
        }

        protected override async Task HandleRequirementAsync(
            AuthorizationHandlerContext context, 
            ShouldBeActive requirement)
        {
            var userName = context.User.GetUserId();
            if(!string.IsNullOrEmpty(userName))
            {
                // get user and check if IsActive is true
                var user = await _userManager.FindByIdAsync(userName);
                if (user.IsActive == requirement.IsActive)
                {
                    context.Succeed(requirement);
                }
                else
                {
                    context.Fail(); // deny access
                }
            }
            else
            {
                context.Fail(); // deny access. attrib should only be used for authorized users.
            }            
        }
    }
}
