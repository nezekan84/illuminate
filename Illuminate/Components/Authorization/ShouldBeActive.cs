﻿using Microsoft.AspNetCore.Authorization;

namespace Illuminate.Components.Authorization
{
    public class ShouldBeActive : IAuthorizationRequirement
    {
        public const string POLICY_NAME = "ShouldBeActive";

        public bool IsActive { get; } = true;
    }
}
