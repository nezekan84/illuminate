﻿using Microsoft.AspNetCore.Authorization;

namespace Illuminate.Components.Authorization
{
    /// <summary>
    /// Authorize user based on chosen constructor overload
    /// </summary>
    public class AuthorizeRoles : AuthorizeAttribute
    {
        /// <summary>
        /// Authorize user using given roles and ShouldBeActive policy
        /// </summary>
        /// <param name="roles"></param>
        public AuthorizeRoles(params string[] roles)
        {
            Merge(roles);
        }

        /// <summary>
        /// Authorize user using given roles and ShouldBeActive policy
        /// </summary>
        /// <param name="includeAdmin">include admin role</param>
        /// <param name="roles"></param>
        public AuthorizeRoles(bool includeAdmin, params string[] roles)
        {
            Merge(roles);

            if (includeAdmin)
                Roles = string.Join(',', Roles, UserPermissions.Admin);
        }

        /// <summary>
        /// Authorize user using only ShouldBeActive policy
        /// </summary>
        public AuthorizeRoles()
        {
            Policy = ShouldBeActive.POLICY_NAME;
        }

        /// <summary>
        /// Merge roles and add ShouldBeActive policy to requirements.
        /// </summary>
        /// <param name="roles"></param>
        private void Merge(string[] roles)
        {
            Roles = string.Join(',', roles);
            Policy = ShouldBeActive.POLICY_NAME;
        }
    }
}
