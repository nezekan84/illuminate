﻿using System.ComponentModel.DataAnnotations;

namespace Illuminate.Components.Authorization
{
    public class UserPermissions
    {
        /// <summary>
        /// has access to everything
        /// </summary>
        [Display(Name = "Admin", GroupName = "Administration", Description = "Grants access to the entire app.")]
        public const string Admin = "Admin";

        /// <summary>
        /// Allow user to add/remove claims
        /// </summary>
        [Display(Name = "Manage Permissions", GroupName = "Permissions", Description = "Grants access to managing user permissions.")]
        public const string ManagePermissions = "ManagePermissions";

        /// <summary>
        /// Manage users
        /// </summary>
        [Display(Name = "Manage Users", GroupName = "Users", Description = "Grants access to user management functions.")]
        public const string ManageUsers = "ManageUsers";

        /// <summary>
        /// 
        /// </summary>
        [Display(Name = "Manage Branches", GroupName = "Branches", Description = "Grants access to branch management functions.")]
        public const string ManageBranches = "ManageBranches";

        /// <summary>
        /// Create, update
        /// </summary>
        [Display(Name = "Create and Update Stocks", GroupName = "Stocks", Description = "Enables creating and updating of stocks.")]
        public const string CreateUpdateStocks = "CreateStocks";

        /// <summary>
        /// Assign stocks to branch
        /// </summary>
        [Display(Name = "Add Stocks", GroupName = "Stocks", Description = "Enables assigning of stocks to branches.")]
        public const string AddStocks = "AddStocks";

        /// <summary>
        /// Move stocks to other branch
        /// </summary>
        [Display(Name = "Move Stocks", GroupName = "Stocks", Description = "Enables stock transfer.")]
        public const string MoveStocks = "MoveStocks";

        /// <summary>
        /// Pull stock (sell?)
        /// </summary>
        [Display(Name = "Pull Stocks", GroupName = "Stocks", Description = "Enables stock pull out.")]
        public const string PullStocks = "PullStocks";

        /// <summary>
        /// Activate/deactivate stocks
        /// </summary>
        [Display(Name = "Activate Stocks", GroupName = "Stocks", Description = "Enables stock activation and deactivation.")]
        public const string ActivateStocks = "ActivateStocks";

        /// <summary>
        /// List and view stocks
        /// </summary>
        [Display(Name = "List and View stocks", GroupName = "Stocks", Description = "Enables listing and viewing of stocks.")]
        public const string ListAndViewStocks = "ListAndViewStocks";

        /// <summary>
        /// 
        /// </summary>
        [Display(Name = "Edit App Settings", GroupName = "Settings", Description = "Enables management of app settings.")]
        public const string EditAppSettings = "EditAppSettings";
    }
}
